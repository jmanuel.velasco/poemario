export const generateSlug = (str: string) =>
    str
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/[,.?¿!¡]/g, '')
        .replace(/ /g, '-')
        .toLocaleLowerCase();
