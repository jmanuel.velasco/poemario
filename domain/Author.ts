export interface Author {
  id?: number | string;
  nombre?: string;
  seudonimo?: string;
  email?: string;
  password?: string;
}
