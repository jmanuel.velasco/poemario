import { Author } from "./Author";

export interface AuthorRepository {
  getById(id: string): Promise<Author | undefined>;
  list(): Promise<Author[] | undefined>;
  registerWithAuthProviderByEmail<T>(
    author: Author,
    authenticatorFn: () => Promise<T>,
    authenticatorUserIdGetter: (authenticatedUserPayload: T) => number | string
  ): Promise<{ author: Author } | undefined>;
  login<T>(
    author: Author,
    loginFn: () => Promise<T>,
    loginUserIdGetter: (authenticatedUserPayload: T) => number | string
  ): Promise<{ author: Author } | undefined>;
  delete(author: Author): Promise<void>;
  update(author: Author): Promise<Author>;
}
