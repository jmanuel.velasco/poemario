import { Musa } from "./Musa";

export interface MusaRepository {
  list(): Promise<Musa[]>;
  listAll(): Promise<Musa[]>;
  get(musaId: number): Promise<Musa | undefined>;
  create(musa: Musa): Promise<Musa>;
}
