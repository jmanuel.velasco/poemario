import { Publication } from "./Publication";

export interface PublicationRepository {
  listByMusa(musaId: number): Promise<Publication[]>;
  listByAuthor(authorId: string): Promise<Publication[]>;
  findById(publication: Publication): Promise<Publication | undefined>;
  delete(publication: Publication): Promise<Publication>;
  update(publication: Publication): Promise<Publication>;
  create(publication: Publication): Promise<Publication>;
}
