import { Author } from "./Author";
import { Musa } from "./Musa";

export interface Publication {
  id: number;
  slug: string;
  title: string;
  body: string;
  state: number;
  musa: Musa[];
  author: Author;
}
