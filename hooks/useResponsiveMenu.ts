import { RefObject, useCallback, useEffect, useState } from "react";

export const useResponsiveMenu = (container: RefObject<HTMLDivElement>) => {
  const [responsiveMenu, setResponsiveMenu] = useState(false);

  const handleResponsiveMenu = useCallback(() => {
    const containerDimensions = container.current
      ? container.current.getBoundingClientRect()
      : null;

    if (containerDimensions && containerDimensions?.width < 900) {
      setResponsiveMenu(true);
    } else {
      setResponsiveMenu(false);
    }
  }, [container]);

  useEffect(() => {
    handleResponsiveMenu();
    function handleResize() {
      handleResponsiveMenu();
    }
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  });

  return {
    responsiveMenu,
  };
};
