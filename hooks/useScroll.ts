import { useCallback, useEffect, useState } from "react";

export type useScrollProps = {
  displayScroller: boolean;
  scrollerTop: number;
  scrollerLeft: number;
  handleScroll: (event: React.UIEvent<HTMLElement>) => void;
  setScrollToTop: () => void;
};

const isScrollerAtBottom = (elt: HTMLElement) => {
  return (
    Math.floor(Number(elt?.scrollHeight) - Number(elt?.scrollTop)) >
    Number(elt?.clientHeight)
  );
};

export function useScroll(container: HTMLElement): useScrollProps {
  const [displayScroller, setDisplayScroller] = useState(true);
  const [scrollerTop, setScrollerTop] = useState(0);
  const [scrollerLeft, setScrollerLeft] = useState(0);

  useEffect(() => {
    const containerDimensions = container
      ? container.getBoundingClientRect()
      : null;
    const left =
      (containerDimensions?.x || 0) + (containerDimensions?.width || 0) - 40;
    const top = containerDimensions?.height || 0;
    setScrollerTop(top);
    setScrollerLeft(left);

    setDisplayScroller(isScrollerAtBottom(container));
  });

  useEffect(() => {
    function handleResize() {
      const containerDimensions = container
        ? container.getBoundingClientRect()
        : null;
      const left =
        (containerDimensions?.x || 0) + (containerDimensions?.width || 0);
      const top = containerDimensions?.height || 0;
      setScrollerTop(top);
      setScrollerLeft(left);
    }

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  });

  const handleScroll = (event: React.UIEvent<HTMLElement>) => {
    const elt = event.target as HTMLElement;
    setDisplayScroller(isScrollerAtBottom(elt));
  };

  const setScrollToTop = useCallback(() => {
    if (container) {
      container.scrollTop = 0;
    }
  }, [container]);

  return {
    displayScroller,
    scrollerTop,
    scrollerLeft,
    handleScroll,
    setScrollToTop,
  };
}
