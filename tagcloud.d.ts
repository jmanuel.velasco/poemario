// Type definitions for [~THE LIBRARY NAME~] [~OPTIONAL VERSION NUMBER~]
// Project: [~THE PROJECT NAME~]
// Definitions by: [~YOUR NAME~] <[~A URL FOR YOU~]>
/*~ If this library is callable (e.g. can be invoked as TagCanvas(3)),
 *~ include those call signatures here.
 *~ Otherwise, delete this section.
 */
declare function TagCanvas(a: string): string;
declare function TagCanvas(a: number): number;
/*~ If you want the name of this library to be a valid type name,
 *~ you can do so here.
 *~
 *~ For example, this allows us to write 'var x: TagCanvas';
 *~ Be sure this actually makes sense! If it doesn't, just
 *~ delete this declaration and add types inside the namespace below.
 */
interface TagCanvas {
    name: string;
    length: number;
    extras?: string[];
    wheelZoom: boolean;
}
/*~ If your library has properties exposed on a global variable,
 *~ place them here.
 *~ You should also place types (interfaces and type alias) here.
 */
declare namespace TagCanvas {
    // //~ We can write 'TagCanvas.timeout = 50;'
    // let timeout: number;
    // let wheelZoom: boolean;
    // //~ We can access 'TagCanvas.version', but not change it
    // const version: string;
    // //~ There's some class we can create via 'let c = new TagCanvas.Cat(42)'
    // //~ Or reference e.g. 'function f(c: TagCanvas.Cat) { ... }
    // class Cat {
    //     constructor(n: number);
    //     //~ We can read 'c.age' from a 'Cat' instance
    //     readonly age: number;
    //     //~ We can invoke 'c.purr()' from a 'Cat' instance
    //     purr(): void;
    // }
    // //~ We can declare a variable as
    // //~   'var s: TagCanvas.CatSettings = { weight: 5, name: "Maru" };'
    // interface CatSettings {
    //     weight: number;
    //     name: string;
    //     tailLength?: number;
    // }
    // //~ We can write 'const v: TagCanvas.VetID = 42;'
    // //~  or 'const v: TagCanvas.VetID = "bob";'
    // type VetID = string | number;
    // //~ We can invoke 'TagCanvas.checkCat(c)' or 'TagCanvas.checkCat(c, v);'
    // function checkCat(c: Cat, s?: VetID);

    let wheelZoom: boolean;
    let textFont: string;
    let textColour: string;
    let textHeight: number;
    let outlineMethod: string;
    let outlineIncrease: number;
    let maxSpeed: number;
    let minSpeed: number;
    let minBrightness: number;
    let depth: number;
    let pulsateTo: number;
    let initial: [number, number];
    let decel: number;
    let reverse: boolean;
    let hideTags: boolean;
    let shadow: boolean;
    let shadowBlur: number;
    let weight: boolean;
    let imageScale: boolean | null;
    let fadeIn: number;
    let clickToFront: number;
    let width: number;
    let height: number;
    let interval: number;
    let lock: 'x' | 'y';

    function Start(string, string, {});
}
