import { SignJWT } from "jose";

export const createToken = async (payload: { [key: string]: string }) => {
  const jwtToken = await new SignJWT({ id: payload.id })
    .setProtectedHeader({ alg: "HS256" })
    .setIssuedAt()
    .setExpirationTime("1d")
    .sign(new TextEncoder().encode(process.env.SECRET_TOKEN));

  return jwtToken;
};