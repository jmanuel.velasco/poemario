import { Musa } from "../domain/Musa";
import { MusaRepository } from "../domain/MusaRepository";
import { db } from "./mysqlConnect";

export class MysqlMusaRepository implements MusaRepository {
  async get(musaId: number): Promise<Musa | undefined> {
    try {
      const row: any = await db.query({
        sql: `SELECT id, name
                FROM musas
                WHERE id = ?`,
        values: [musaId],
      });

      db.end();
      if (row.length === 0) {
        return undefined;
      }
      

      return {
        id: row[0]["id"],
        name: row[0]["name"],
      };
    } catch (error) {
      throw new Error(`MYSQL_MUSAREPOSITORY::GET: ${error}`);
    }
  }
  async list(): Promise<Musa[]> {
    try {
      const rows: any = await db.query(`
            SELECT DISTINCT
                (musas.id), musas.name
            FROM
                musas
                    JOIN
                publications_musas pm ON pm.id_musa = musas.id
                    JOIN
                publications p ON p.id = pm.id_publication
            WHERE p.id_state = 1
        `);
      db.end();

      const musas: Musa[] = rows.map((row: any) => ({
        id: row["id"],
        name: row["name"],
      }));

      return musas;
    } catch (error) {
      throw new Error(`MYSQL_MUSAREPOSITORY::LIST: ${error}`);
    }
  }
  async listAll(): Promise<Musa[]> {
    try {
      const rows: any = await db.query(
        `SELECT  id, name FROM musas`
      );
      db.end();

      const musas: Musa[] = rows.map((row: any) => ({
        id: row["id"],
        name: row["name"],
      }));

      return musas;
    } catch (error) {
      throw new Error(`MYSQL_MUSAREPOSITORY::LISTALL: ${error}`);
    }
  }
  async create(musa: Musa): Promise<Musa> {
    try {
      const result: any = await db.query({
        sql: `INSERT INTO musas (name) VALUES (?)`,
        values: [musa.name],
      });

      db.end();

      return {
        id: result.insertId,
        name: musa.name,
      };
    } catch (error) {
      throw new Error(`MYSQL_MUSAREPOSITORY::CREATE: ${error}`);
    }
  }
}
