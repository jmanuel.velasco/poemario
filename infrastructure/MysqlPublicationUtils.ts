import { db } from "./mysqlConnect";

export const getPublicationId = async ({
  writtingId,
}: {
  writtingId: number;
}) => {
  const resultPublication: any = await db.query({
    sql: `SELECT id FROM publications WHERE (id_writting = ?)`,
    values: [writtingId],
  });

  const idPublication = resultPublication[0].id;
  return idPublication;
};
