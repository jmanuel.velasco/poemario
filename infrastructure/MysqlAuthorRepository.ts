// TODO: Avoid FirebaseError dependency here
import { FirebaseError } from "firebase/app";
import { Author } from "../domain/Author";
import { AuthorRepository } from "../domain/AuthorRepository";
import { db } from "./mysqlConnect";

export class MysqlAuthorRepository implements AuthorRepository {
  async getById(id: string): Promise<Author | undefined> {
    try {
      const row: any = await db.query({
        sql: `SELECT id, email, nombre, seudonimo FROM users where id = ?;`,
        values: [`${id}`],
      });

      db.end();

      if (row.length === 0) {
        return undefined
      }

      return {
        id: row[0]["id"],
        email: row[0]["email"],
        nombre: row[0]["nombre"],
        seudonimo: row[0]["seudonimo"],
      }
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      }
    }
  }

  async list(): Promise<Author[] | undefined> {
    try {
      const results: any = await db.query({
        sql: `SELECT id, email, nombre, seudonimo FROM users;`,
      });

      db.end();

      const authors: Author[] = results.map((row: { [x: string]: string }) => {
        return {
          id: row["id"],
          email: row["email"],
          nombre: row["nombre"],
          seudonimo: row["seudonimo"],
        };
      });

      return authors;
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      }
    }
  }

  async registerWithAuthProviderByEmail<T>(
    author: Author,
    authenticatorFn: () => Promise<T>,
    authenticatorUserIdGetter: (authenticatedUserPayload: T) => string | number
  ): Promise<{ author: Author } | undefined> {
    // Check if user exists with the provided seudonimo
    const authors = await this.list();

    const exists = authors?.some((a) => a.seudonimo === author.seudonimo);
    if (exists || !author.email || !author.password) {
      throw new Error(`auth/seudonimo-already-in-use`);
    }

    try {
      // Perform authentication through external authentication provider service
      const registeredAuthor = await authenticatorFn();
      const userId = authenticatorUserIdGetter(registeredAuthor);

      // Register the user in database
      await db.query({
        sql: `INSERT INTO users (id, nombre, seudonimo, email) VALUES (?, ?, ?, ?)`,
        values: [
          `${userId}`,
          `${author.nombre}`,
          `${author.seudonimo}`,
          `${author.email}`,
        ],
      });

      db.end();

      return {
        author: {
          id: userId,
          nombre: author.nombre,
          seudonimo: author.seudonimo,
          email: author.email,
        },
      };
    } catch (error) {
      if (error instanceof FirebaseError) {
        throw new Error(error.code);
      }
      if (error instanceof Error) {
        throw new Error(error.message);
      }
    }
  }

  async login<T>(
    author: Author,
    loginFn: () => Promise<T>,
    loginUserIdGetter: (authenticatedUserPayload: T) => number | string
  ): Promise<{ author: Author } | undefined> {
    try {
      // Perform login through external authenticator provider service
      const loggedAuthor = await loginFn();
      const userId = loginUserIdGetter(loggedAuthor);

      // Check if the user exists in the application database
      const authors = await this.list();
      const authorDb = authors?.find((a) => a.id === userId);

      if (authorDb) {
        return {
          author: {
            id: userId,
            nombre: authorDb.nombre,
            seudonimo: authorDb.seudonimo,
            email: author.email,
          },
        };
      }
    } catch (error) {
      if (error instanceof FirebaseError) {
        throw new Error(error.code);
      }
      if (error instanceof Error) {
        throw new Error(error.message);
      }
    }
  }

  async delete(author: Author): Promise<void> {
    try {
      await db.query({
        sql: `DELETE FROM users WHERE (id = ?)`,
        values: [`${author.id}`],
      });

      db.end();
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      }
    }
  }

  async update(author: Author): Promise<Author> {
    const { id, nombre, seudonimo } = author;
    try {
      await db.query({
        sql: `UPDATE users SET nombre = ?, seudonimo = ? WHERE (id = ?)`,
        values: [`${nombre}`, `${seudonimo}`, `${id}`],
      });

      db.end();

      return {
        id,
        nombre,
        seudonimo,
      };
    } catch (error) {
      if (error instanceof Error) {
        if (error instanceof Error) {
          throw new Error(error.message);
        }
      }
      return author;
    }
  }
}
