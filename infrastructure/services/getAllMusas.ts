
export const getAllMusas = async () => {
  const fetchgetAllMusa = await fetch("/api/musas", {
    method: "GET",
  });
  if (fetchgetAllMusa.status === 200) {
    const allMusasList = await fetchgetAllMusa.json();
    return { data: allMusasList.data };
  } else {
    return { error: "Ha habido un error obteniendo el listado de musas" };
  }
};
