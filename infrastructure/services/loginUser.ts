export const loginUser = async ({
  email,
  password,
}: {
  email: string;
  password: string;
}) => {
  try {
    const fetchLoginAuthor = await fetch("/api/user/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    });

    const loginAuthorResponse = await fetchLoginAuthor.json();
    if (fetchLoginAuthor.status === 200) {
      return loginAuthorResponse.data;
    } else {
      return { error: loginAuthorResponse };
    }
  } catch (error) {
    if (error instanceof Error) {
      throw new Error(error.message);
    }
  }
};
