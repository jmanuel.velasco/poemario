import { Author } from "../../domain/Author";
import { Musa } from "../../domain/Musa";
import { Publication } from "../../domain/Publication";

export const enum STATUS {
  PUBLISHED = 1,
  DRAFT = 2,
}

export const defaultPublicationData = (author: Author) => {
  return {
    id: 0,
    slug: "",
    title: "",
    body: "",
    state: STATUS.DRAFT,
    musa: [],
    author,
  };
};

export const createPublication = async ({
  publication,
}: {
  publication: Publication;
}) => {
  const fetchCreate = await fetch("/api/publication", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ publication }),
  });
  const fetchCreateResponse = await fetchCreate.json();
  if (fetchCreate.status === 201) {
    return { data: fetchCreateResponse.data };
  } else {
    if (fetchCreateResponse.message === '"exp" claim timestamp check failed') {
      return { error: "JWT expired" };
    }
    return { error: "Ha ocorrido un error creando la publicación" };
  }
};

export const updatePublication = async ({
  publication,
}: {
  publication: Publication;
}) => {
  const fetchUpdate = await fetch("/api/publication", {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      publication,
    }),
  });
  const fetchUpdateResponse = await fetchUpdate.json();
  if (fetchUpdate.status === 200) {
    return { data: fetchUpdateResponse.data };
  } else {
    if (fetchUpdateResponse.message === '"exp" claim timestamp check failed') {
      return { error: "JWT expired" };
    }
    return { error: "Ha ocorrido un error creando la publicación" };
  }
};

export const deletePublication = async ({
  publication,
}: {
  publication: Publication;
}) => {
  const fetchDelete = await fetch("/api/publication", {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      publication,
    }),
  });
  const fetchDeleteResponse = await fetchDelete.json();
  if (fetchDelete.status === 200) {
    return { data: fetchDeleteResponse.data };
  } else {
    if (fetchDeleteResponse.message === '"exp" claim timestamp check failed') {
      return { error: "JWT expired" };
    }
    return { error: "Ha ocorrido un error creando la publicación" };
  }
};

export const getPublicationsByAuthor = async (author: Author) => {
  const authorPublicationsFetch = await fetch(
    `/api/user/${author.id}/publications`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  const authorPublicationResponse = await authorPublicationsFetch.json();
  if (authorPublicationsFetch.status === 200) {
    const publications = authorPublicationResponse.data.map(
      (publication: Publication) => {
        return { ...publication, author: author };
      }
    );
    return { data: publications };
  } else {
    if (
      authorPublicationResponse.message === '"exp" claim timestamp check failed'
    ) {
      return { error: "JWT expired" };
    }
    return { error: "Ha ocorrido un error creando la publicación" };
  }
};

export const getPublicationsByMusa = async (
  musaId: number
): Promise<Publication[]> => {
  const authorPublicationsFetch = await fetch(
    `/api/musas/${musaId}/publications`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  if (authorPublicationsFetch.status === 200) {
    const authorPublicationResponse = await authorPublicationsFetch.json();
    const publications = authorPublicationResponse.data.map(
      (publication: Publication) => {
        return { ...publication };
      }
    );
    return publications;
  }
  return [];
};

export const isMusaAlreadyInPublication = ({
  publication,
  musa,
}: {
  publication: Publication;
  musa: Musa;
}) =>
  publication.musa.some((publicationMusa) => publicationMusa.id === musa.id);
