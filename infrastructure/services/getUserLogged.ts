import Cookies from "js-cookie";

export const getUserLogged = async () => {
  const authorId = Cookies.get("authorId");
  if (!authorId) {
    return false;
  }
  
  try {
    const fetchUser = await fetch(`/api/user/${authorId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const { data: user } = await fetchUser.json();
    return { data: user };
  } catch (error) {
    if (error instanceof Error) {
      return {
        error: "Ha habido un error recuperando la información del usuario",
      };
    }
  }
};
