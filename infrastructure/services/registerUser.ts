import { Author } from "./../../domain/Author";

export const registerUser = async ({ userData }: { userData: Author }) => {
  const { email, password, nombre, seudonimo } = userData;
  try {
    const fetchRegisterAuthor = await fetch("/api/user/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre,
        seudonimo,
        email,
        password,
      }),
    });

    const registerAuthorResponse = await fetchRegisterAuthor.json();

    if (fetchRegisterAuthor.status === 201) {
      return registerAuthorResponse.data;
    } else {
      return { error: registerAuthorResponse };
    }
  } catch (error) {
    if (error instanceof Error) {
      throw new Error(error.message);
    }
  }
};
