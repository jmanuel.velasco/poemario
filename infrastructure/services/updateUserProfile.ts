import { Author } from "../../domain";

export const updateUserProfile = async ({ author }: { author: Author }) => {
  try {
    const updateFetch = await fetch(`/api/user/${author.id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nombre: author.nombre,
        seudonimo: author.seudonimo,
      }),
    });
    const updatedUser = await updateFetch.json();
    if (updateFetch.status === 200) {
      return { data: author };
    } else {
      const { message } = updatedUser;
      return { error: message };
    }
  } catch (error) {
    if (error instanceof Error) {
      return { error: `Error en el proceso de actualización.` };
    }
  }
};
