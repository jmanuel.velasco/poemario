export const createMusa = async ({ name }: { name: string }) => {
  try {
    const fetchCreateMusa = await fetch("/api/musas/create", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name }),
    });
    const fetchCreateMusaResponse = await fetchCreateMusa.json();
    if (fetchCreateMusa.status === 201) {
      return fetchCreateMusaResponse;
    } else {
      if (
        fetchCreateMusaResponse.message === '"exp" claim timestamp check failed'
      ) {
        return { error: "JWT expired" };
      }
      return {
        error: "Se ha producido un error creando la musa que has elegido",
      };
    }
  } catch (error) {
    if (error instanceof Error) {
      throw new Error(error.message);
    }
  }
};
