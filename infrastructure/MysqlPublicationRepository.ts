import { Musa, Publication, PublicationRepository } from "../domain";
import { generateSlug } from "../lib/string.utils";
import { getPublicationId } from "./MysqlPublicationUtils";
import { db } from "./mysqlConnect";

export class MysqlPublicationRepository implements PublicationRepository {
  async listByAuthor(authorId: string): Promise<Publication[]> {
    try {
      const rows: any = await db.query({
        sql: `SELECT 
                w.id, w.title, w.body, p.id_state, GROUP_CONCAT(CONCAT(m.id,':',m.name)) as musas_list
                    FROM
                publications p
                    JOIN
                writtings w ON w.id = p.id_writting
                    JOIN
                publications_musas pm ON pm.id_publication = p.id
                    JOIN
                musas m ON m.id = pm.id_musa
                    WHERE
                p.id_author = ?
                    GROUP BY w.id, w.title, w.body, p.id_state
              `,
        values: [authorId],
      });

      db.end();

      const publications: Publication[] = rows.map(
        (row: { [x: string]: string }) => {
          const musas = row["musas_list"].split(",");
          const musasList = musas.map((musa) => {
            const musaObj = musa.split(":");
            return {
              id: Number(musaObj[0]),
              name: musaObj[1],
            };
          });
          return {
            id: row["id"],
            title: row["title"],
            body: row["body"],
            state: row["id_state"],
            musa: musasList,
            slug: generateSlug(row["title"]),
          };
        }
      );

      return publications;
    } catch (error) {
      throw new Error(
        `MYSQLPUBLICATIONRESPOSITORY::LISTPUBLICATIONSOFAUTHOR: ${error}`
      );
    }
  }
  async listByMusa(musaId: number): Promise<Publication[]> {
    try {
      const rows: any = await db.query({
        sql: `SELECT 
                  w.id AS writtingId,
                  w.title,
                  w.body,
                  m.id AS musa_id,
                  m.name AS musa_title,
                  u.nombre AS name,
                  u.seudonimo,
                  u.email,
                  u.id AS authorId
              FROM
                  publications p
                      JOIN
                  writtings w ON w.id = p.id_writting
                      JOIN
                  publications_musas pm ON pm.id_publication = p.id
                      JOIN
                  musas m ON m.id = pm.id_musa
                      JOIN
                  users u ON u.id = p.id_author
              WHERE
                  p.id_state = 1 AND pm.id_musa = ?`,
        values: [musaId],
      });

      db.end();

      const publications: Publication[] = rows.map(
        (row: { [x: string]: string }) => ({
          id: row["writtingId"],
          title: row["title"],
          body: row["body"],
          slug: generateSlug(row["title"]),
          author: {
            id: row["authorId"],
            nombre: row["name"],
            seudonimo: row["seudonimo"],
            email: row["email"],
          },
        })
      );

      return publications;
    } catch (error) {
      throw new Error(
        `MYSQLPUBLICATIONRESPOSITORY::LISTPUBLICATIONSOFMUSA: ${error}`
      );
    }
  }

  async findById(publication: Publication): Promise<Publication | undefined> {
    try {
      const row: any = await db.query({
        sql: `SELECT 
                w.id, w.title, w.body, p.id_state, GROUP_CONCAT(CONCAT(m.id,':',m.name)) as musas_list, p.id_author
                    FROM
                publications p
                    JOIN
                writtings w ON w.id = p.id_writting
                    JOIN
                publications_musas pm ON pm.id_publication = p.id
                    JOIN
                musas m ON m.id = pm.id_musa
                    WHERE
                w.id = ?
                    GROUP BY w.id, w.title, w.body, p.id_state, p.id_author
              `,
        values: [publication.id],
      });

      if (row.length === 0) {
        return undefined;
      }

      const musas = row[0]["musas_list"].split(",");
      const musasList: Musa[] = musas.map((musa: string) => {
        const musaObj = musa.split(":");
        return {
          id: Number(musaObj[0]),
          name: musaObj[1],
        };
      });

      return {
        id: row[0]["id"],
        title: row[0]["title"],
        body: row[0]["body"],
        state: row[0]["id_state"],
        musa: musasList,
        slug: generateSlug(row[0]["title"]),
        author: {
          id: row[0]["id_author"],
        },
      };
    } catch (error) {
      throw new Error(`MYSQLPUBLICATIONRESPOSITORY::FINBYID: ${error}`);
    }
  }

  async delete(publication: Publication): Promise<Publication> {
    try {
      const idPublication = await getPublicationId({
        writtingId: publication.id,
      });
      await db
        .transaction()
        .query("SET FOREIGN_KEY_CHECKS = 0")
        .query("DELETE FROM publications WHERE (id = ?)", [idPublication])
        .query("DELETE FROM publications_musas WHERE (id_publication = ?)", [
          idPublication,
        ])
        .query("DELETE FROM writtings WHERE (id = ?)", [publication.id])
        .query("SET FOREIGN_KEY_CHECKS = 1")
        .commit(); // execute the queries

      db.end();

      return publication;
    } catch (error) {
      throw new Error(`MYSQLPUBLICATIONRESPOSITORY::DELETE: ${error}`);
    }
  }

  async update(publication: Publication): Promise<Publication> {
    const parsedBody = publication.body.replace(/\n/g, "<br />");
    try {
      const result: any = await db.query({
        sql: `UPDATE writtings 
          SET title = ?, 
              body = ?
          WHERE (id = ?)`,
        values: [publication.title, parsedBody, publication.id],
      });

      if (result.affectedRows === 1) {
        const payload = {
          writtingId: publication.id,
        };
        const idPublication = await getPublicationId(payload);

        await db.query({
          sql: `UPDATE publications SET id_state = ? WHERE (id = ?)`,
          values: [publication.state, idPublication],
        });

        await db.query({
          sql: `DELETE FROM publications_musas WHERE (id_publication = ?)`,
          values: [idPublication],
        });

        publication.musa.forEach(async (musa: Musa) => {
          await db.query({
            sql: `INSERT IGNORE INTO publications_musas (id_publication, id_musa) VALUES (?, ?)`,
            values: [idPublication, musa.id],
          });
        });
      }

      db.end();

      return {
        ...publication,
        body: parsedBody,
      };
    } catch (error) {
      throw new Error(`MYSQLPUBLICATIONRESPOSITORY::UPDATE: ${error}`);
    }
  }

  async create(publication: Publication): Promise<Publication> {
    try {
      const resultWritting: any = await db.query({
        sql: `INSERT INTO writtings (title, body) VALUES (?, ?)`,
        values: [publication.title, publication.body],
      });

      const writtingId = resultWritting.insertId;

      const resultPublication: any = await db.query({
        sql: `INSERT INTO publications (id_writting, id_state, id_author) VALUES (?, 2, ?)`,
        values: [writtingId, publication.author.id],
      });

      publication.musa.forEach(async (musa: Musa) => {
        await db.query({
          sql: `INSERT INTO publications_musas (id_publication, id_musa) VALUES (?, ?)`,
          values: [resultPublication.insertId, musa.id],
        });
      });

      db.end();

      return {
        id: writtingId,
        slug: generateSlug(publication.title),
        title: publication.title,
        body: publication.body,
        state: publication.state,
        musa: publication.musa,
        author: publication.author,
      };
    } catch (error) {
      throw new Error(`MYSQLPUBLICATIONRESPOSITORY::CREATE: ${error}`);
    }
  }
}
