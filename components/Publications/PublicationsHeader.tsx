import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Box } from "@mui/material";
import Link from "next/link";
import homeStyles from "../../styles/home.module.css";

interface PublicationsHeaderProps {
  musa: string;
}

export const PublicationsHeader = ({ musa }: PublicationsHeaderProps) => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        flexWrap: "nowrap",
      }}
    >
      <Link href={"/musas"}>
        <ArrowBackIcon className={`${homeStyles.arrowIcon}`} />
      </Link>
      <Box
        sx={{
          display: "flex",
          alignItems: "baseline",
          justifyContent: "end",
          flexWrap: "wrap"
        }}
      >
        <Link href={"/"}>
          <h1 className={`${homeStyles.pageTitle}`}>Amimusa</h1>
        </Link>
        <span className={`${homeStyles.pageSubtitle}`}>{musa}</span>
      </Box>
    </Box>
  );
};
