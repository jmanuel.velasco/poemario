import { RefObject } from "react";
import { Publication } from "../../../domain/Publication";
import { useResponsiveMenu } from "../../../hooks/useResponsiveMenu";
import { HamburguerWrapper } from "./HamburguerWrapper";
import { MenuIndex } from "./MenuIndex";

export interface MenuProps {
  container: RefObject<HTMLDivElement>;
  publications: Publication[];
}

export const Menu = ({ container, publications }: MenuProps) => {
  const { responsiveMenu } = useResponsiveMenu(container);

  return responsiveMenu ? (
    <HamburguerWrapper publications={publications} />
  ) : (
    <MenuIndex publications={publications} />
  );
};
