import { Box } from "@mui/material";
import { useContext, useRef } from "react";
import { ApplicationContext } from "../../../contexts/ApplicationContext/ApplicationContext";
import { UIContext } from "../../../contexts/UIContext/UIContext";
import { Publication } from "../../../domain/Publication";
import { useScroll } from "../../../hooks/useScroll";
import publicationStyles from "../../../styles/publications.module.css";
import { Contents } from "./Contents";

export interface MenuIndexProps {
  publications: Publication[];
  isHamburguer?: boolean;
}

export function MenuIndex({
  publications,
  isHamburguer = false,
}: MenuIndexProps) {
  const { isUserLogged } = useContext(ApplicationContext);
  const { isDashboardMenu } = useContext(UIContext);
  const containerRef = useRef<HTMLUListElement>(null);
  const {
    displayScroller,
    scrollerTop,
    scrollerLeft,
    handleScroll,
    setScrollToTop,
  } = useScroll(containerRef.current as HTMLUListElement);

  return (
    <>
      <Box
        className={`
          ${publicationStyles.hideScrollBar}
          ${
            isHamburguer
              ? publicationStyles.hamburguerContainer
              : publicationStyles.container
          }
          ${
            isUserLogged && !isDashboardMenu
              ? publicationStyles.headerContainer
              : ""
          }
        `}
        onScroll={handleScroll}
        ref={containerRef}
      >
        <Contents
          publications={publications}
          displayScroller={displayScroller}
          scrollerLeft={scrollerLeft}
          setScrollToTop={setScrollToTop}
        />
      </Box>
    </>
  );
}
