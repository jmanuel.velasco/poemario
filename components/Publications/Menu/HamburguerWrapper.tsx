import SortByAlphaIcon from "@mui/icons-material/SortByAlpha";
import { Box } from "@mui/material";
import { useContext, useEffect } from "react";
import { ApplicationContext } from "../../../contexts";
import { UIContext } from "../../../contexts/UIContext/UIContext";
import { Publication } from "../../../domain/Publication";
import publicationStyles from "../../../styles/publications.module.css";
import { MenuIndex } from "./MenuIndex";

export interface PublicationsHamburguerMenuProps {
  publications: Publication[];
}

export function HamburguerWrapper({
  publications,
}: PublicationsHamburguerMenuProps) {
  const { displayMenu, setDisplayMenu } = useContext(UIContext);
  const { selectPublication } = useContext(ApplicationContext);

  useEffect(() => {
    setDisplayMenu(true);
  }, [setDisplayMenu]);

  const handleOpenPublicationsMenu = () => {
    setDisplayMenu(true);
    selectPublication(null);
  };

  return (
    <>
      <Box className={`${publicationStyles.hamburguerMenu}`}>
        {!displayMenu && (
          <SortByAlphaIcon
            sx={{
              fontSize: "2rem",
              color: "white",
            }}
            onClick={handleOpenPublicationsMenu}
          />
        )}
      </Box>

      <MenuIndex
        publications={displayMenu ? publications : []}
        isHamburguer={true}
      />
    </>
  );
}
