import KeyboardDoubleArrowDownIcon from "@mui/icons-material/KeyboardDoubleArrowDown";
import {
  Box,
  Divider,
  List,
  ListItemButton,
  ListItemText,
} from "@mui/material";
import { purple } from "@mui/material/colors";
import { useContext, useEffect, useState } from "react";
import { ApplicationContext } from "../../../contexts/ApplicationContext/ApplicationContext";
import { UIContext } from "../../../contexts/UIContext/UIContext";
import { Publication } from "../../../domain/Publication";
import publicationStyles from "../../../styles/publications.module.css";

export interface PublicationsIndexContentProps {
  publications: Publication[];
  displayScroller: boolean;
  scrollerLeft: number;
  setScrollToTop: () => void;
}

export const Contents = ({
  publications,
  displayScroller,
  scrollerLeft,
  setScrollToTop,
}: PublicationsIndexContentProps) => {
  const { selectPublication, applicationState, isUserLogged } =
    useContext(ApplicationContext);
  const { setIsNewPublication, setDisplayMenu } = useContext(UIContext);
  const [selectedIndex, setSelectedIndex] = useState(-1);

  const handleSelectPublication = async (
    publication: Publication,
    selectedPublicationIndex: number
  ) => {
    selectPublication(publication);
    setSelectedIndex(selectedPublicationIndex);
    setDisplayMenu(false);
    setIsNewPublication(false);
    // hack to force to calculate if scroll might be displayed
    window.dispatchEvent(new Event('resize'));
  };

  useEffect(() => {
    if (!applicationState.publication) {
      setSelectedIndex(-1);
      setScrollToTop();
    }
  }, [applicationState.publication, setScrollToTop]);

  return (
    <>
      <List dense>
        {publications.map((publication: Publication, idx: number) => {
          return (
            <Box key={publication.id}>
              <ListItemButton
                selected={selectedIndex === idx}
                onClick={() => handleSelectPublication(publication, idx)}
              >
                <ListItemText
                  primary={publication.title.toLocaleUpperCase()}
                  className={`${publicationStyles.indexTitle}`}
                />
              </ListItemButton>

              <Divider
                variant="middle"
                sx={{ backgroundColor: "rgba(0, 0, 0, 1)" }}
              />
            </Box>
          );
        })}
      </List>
      {displayScroller && (
        <KeyboardDoubleArrowDownIcon
          sx={{
            position: "fixed",
            bottom: `${isUserLogged ? "1.5rem" : "4.5rem"}`,
            left: scrollerLeft,
            color: purple[50],
            fontSize: "2rem",
          }}
        />
      )}
    </>
  );
};
