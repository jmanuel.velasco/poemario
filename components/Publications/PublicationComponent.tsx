import KeyboardDoubleArrowDownIcon from "@mui/icons-material/KeyboardDoubleArrowDown";
import { Box, Card, CardActions, CardContent, Typography } from "@mui/material";
import { grey, purple } from "@mui/material/colors";
import parse from "html-react-parser";
import { useContext, useRef } from "react";
import { ApplicationContext } from "../../contexts/ApplicationContext/ApplicationContext";
import { useScroll } from "../../hooks/useScroll";
import publicationStyles from "../../styles/publications.module.css";

export function PublicationComponent({
  isResponsive,
}: {
  isResponsive: boolean;
}) {
  const { applicationState } = useContext(ApplicationContext);
  const { isUserLogged } = useContext(ApplicationContext);
  const containerRef = useRef<HTMLDivElement>(null);

  const { publication } = applicationState;

  const { displayScroller, scrollerTop, scrollerLeft, handleScroll } =
    useScroll(containerRef.current as HTMLDivElement);

  return !isResponsive || publication ? (
    <Box
      ref={containerRef}
      className={`${publicationStyles.hideScrollBar} ${
        publicationStyles.container
      } ${isUserLogged ? publicationStyles.headerContainer : ""}`}
      onScroll={handleScroll}
    >
      <Card
        variant="outlined"
        sx={{
          backgroundColor: "rgba(0,0,0,0)",
          color: grey[400],
          border: "none",
          boxShadow: "none",
        }}
      >
        <CardContent sx={{ textAlign: "center", padding: 0 }}>
          <Typography className={`${publicationStyles.writtingTitle}`}>
            {publication?.title}
          </Typography>
          <Typography
            component="div"
            className={`${publicationStyles.writting}`}
          >
            {parse(publication?.body || "")}
          </Typography>
        </CardContent>

        {publication?.author.nombre && (
          <CardActions
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              // backgroundColor: "#333",
            }}
          >
            <Typography
              variant="h6"
              noWrap
              component="p"
              color="#e4e4e4"
              sx={{
                display: { xs: "none", md: "flex" },
                fontFamily: "Blackout",
                textTransform: "capitalize",
              }}
            >
              {publication?.author.seudonimo}
            </Typography>

            {/* <Button size="small" color="success">
            Like
          </Button> */}
          </CardActions>
        )}
      </Card>
      {displayScroller && (
        <KeyboardDoubleArrowDownIcon
          sx={{
            position: "fixed",
            bottom: `${isUserLogged ? "1.5rem" : "4rem"}`,
            left: scrollerLeft,
            color: purple[50],
            fontSize: "2rem",
          }}
        />
      )}
    </Box>
  ) : null;
}
