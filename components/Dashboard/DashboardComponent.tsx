import PlaylistAddIcon from "@mui/icons-material/PlaylistAdd";
import { Button, Container, Modal, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { useContext, useEffect, useRef } from "react";
import { ApplicationContext } from "../../contexts/ApplicationContext/ApplicationContext";
import { UIContext } from "../../contexts/UIContext/UIContext";
import {
  defaultPublicationData,
  getPublicationsByAuthor,
} from "../../infrastructure/services/publications";
import HeaderComponent from "../Header/HeaderComponent";
import { HamburguerWrapper } from "../Publications/Menu/HamburguerWrapper";
import { Menu } from "../Publications/Menu/Menu";
import { Form } from "./Publication/Form";

export const DashboardComponent = () => {
  const {
    applicationState,
    setPublications,
    selectPublication,
    resetErrors,
    resetMessages,
    displayMessage,
    isSessionExpired,
    logout,
  } = useContext(ApplicationContext);
  const {
    isNewPublication,
    setIsNewPublication,
    stayInPublication,
    setIsDashboardMenu,
    setDisplayMenu,
  } = useContext(UIContext);
  const { author, publication, messages } = applicationState;

  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    resetErrors();
    setIsDashboardMenu(true);
    getPublicationsByAuthor(author).then((publications) => {
      if (publications.data) {
        setPublications(publications.data);
      }
    });
  }, [author, setPublications, setIsDashboardMenu, resetErrors]);

  const publications = applicationState.publications.sort((a, b) =>
    a.title.toLocaleLowerCase() > b.title.toLocaleLowerCase() ? 1 : -1
  );

  const clickNewPublicationBtn = async () => {
    selectPublication(defaultPublicationData(author));
    resetErrors();
    resetMessages();
    setIsNewPublication(true);
    setDisplayMenu(false);
  };

  const clickCloseModalHandler = async () => {
    if (await isSessionExpired()) {
      logout();
    } else {
      resetMessages();
      setIsNewPublication(false);
      if (!stayInPublication) {
        selectPublication(null);
      }
    }
  };

  return publications ? (
    <>
      <HeaderComponent />
      <Container maxWidth="xl" sx={{ marginTop: "0.2rem" }}>
        <Modal
          open={displayMessage}
          onClose={() => resetErrors()}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box
            sx={{
              position: "absolute" as "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              width: "20rem",
              maxWidth: "20rem",
              bgcolor: "background.paper",
              boxShadow: "2.4rem",
              p: 4,
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              gap: 2,
            }}
          >
            <Typography
              id="modal-modal-title"
              align="center"
              variant="h6"
              component="h2"
            >
              {messages}
            </Typography>
            <Button onClick={clickCloseModalHandler}>Entendido</Button>
          </Box>
        </Modal>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Typography
            sx={{ userSelect: "none" }}
            variant="h4"
            fontFamily="BohemianTypewriter"
            gutterBottom
          >
            Mis escritos
          </Typography>
          {!isNewPublication && !publication && (
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <Button
                onClick={clickNewPublicationBtn}
                variant="contained"
                color="success"
                size="small"
                endIcon={<PlaylistAddIcon />}
              >
                Me siento inspirado
              </Button>
            </Box>
          )}
        </Box>

        <Box
          sx={{
            display: { xs: "none", md: "flex" },
            gap: "1rem",
          }}
        >
          <Box sx={{ minWidth: "22rem" }}>
            <Menu container={containerRef} publications={publications} />
          </Box>
          <Form />
        </Box>

        <Box
          sx={{
            display: { xs: "flex", md: "none" },
            gap: "1rem",
          }}
        >
          <Box sx={{ zIndex: 2 }}>
            <HamburguerWrapper publications={publications} />
          </Box>
          <Box sx={{ flex: 1 }}>
            <Form />
          </Box>
        </Box>
      </Container>
    </>
  ) : null;
};
