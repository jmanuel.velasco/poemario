import DeleteIcon from "@mui/icons-material/Delete";
import PublishIcon from "@mui/icons-material/Publish";
import SaveIcon from "@mui/icons-material/Save";
import { Box, Button, Divider, Modal, Stack, Typography } from "@mui/material";
import { useContext, useState } from "react";
import { ApplicationContext } from "../../../contexts/ApplicationContext/ApplicationContext";
import { UIContext } from "../../../contexts/UIContext/UIContext";
import {
  STATUS,
  createPublication,
  deletePublication,
  updatePublication,
} from "../../../infrastructure/services/publications";
import { Publication } from "../../../domain";

interface ControlsProps {
  formValues: any;
}

export const Controls = ({ formValues }: ControlsProps) => {
  const {
    applicationState,
    selectPublication,
    validatePublication,
    addPublication,
    updatePublication: updatePublicationState,
    deletePublication: deletePublicationState,
    addError,
    addMessage,
  } = useContext(ApplicationContext);

  const { isNewPublication, setIsNewPublication, setStayInPublication } =
    useContext(UIContext);

  const { publication } = applicationState;

  const [confirmDelete, setConfirmDelete] = useState(false);

  const saveClickHandler = async () => {
    const publicationData = {
      ...formValues,
      id: publication?.id,
      musa: publication?.musa,
      author: publication?.author,
    } as Publication;
    const isValid = await validatePublication(publicationData);

    if (isValid) {
      const newPublication = await createPublication({
        publication: publicationData,
      });
      if (newPublication.data) {
        addPublication(newPublication.data);
        addMessage("Publicación creada");
        setStayInPublication(true);
      } else {
        if (newPublication.error! === 'JWT expired') {
          addMessage('Sesión expirada');
        }
        addError(newPublication.error!);
      }
    }
  };

  const confirmDeleteClickHandler = () => {
    setConfirmDelete(true);
  };

  const deleteClickHandler = async () => {
    const publicationData = {
      ...formValues,
      id: publication?.id,
      musa: publication?.musa,
    };
    const deletedPublication = await deletePublication({
      publication: publicationData,
    });
    if (deletedPublication.data) {
      deletePublicationState(publicationData);
      addMessage("Publicación borrada");
      setStayInPublication(false);
    } else {
      if (deletedPublication.error! === 'JWT expired') {
        addMessage('Sesión expirada');
      }
      addError(deletedPublication.error!);
    }
  };

  const updateClickHandler = async () => {
    const publicationData = {
      ...formValues,
      id: publication?.id,
      musa: publication?.musa,
    };
    const isValid = await validatePublication(publicationData);

    if (isValid) {
      const updatedPublication = await updatePublication({
        publication: publicationData,
      });
      if (updatedPublication.data) {
        addPublication(updatedPublication.data);
        addMessage("Publicación creada");
        setStayInPublication(true);
      } else {
        addError(updatedPublication.error!);
        if (updatedPublication.error! === 'JWT expired') {
          addMessage('Sesión expirada');
        }
      }
    }
  };

  const publishClickHandler = async (publishPublication: boolean) => {
    if (!publication) return;

    publication.state = publishPublication ? STATUS.PUBLISHED : STATUS.DRAFT;
    const updatedPublication = await updatePublication({ publication });
    if (updatedPublication.data) {
      updatePublicationState(updatedPublication.data);
      addMessage(
        `Publicación ${publishPublication ? "publicada" : "en modo borrador"}`
      );
      setStayInPublication(false);
    } else {
      if (updatedPublication.error! === 'JWT expired') {
        addMessage('Sesión expirada');
      }
      addError(updatedPublication.error!);
    }
  };

  const cancelNewPublicationHandler = () => {
    setIsNewPublication(false);
    selectPublication(null);
  };

  return (
    <Box>
      <Stack direction="row" justifyContent="flex-start">
        {isNewPublication ? (
          <Box sx={{ display: "flex", gap: "1rem" }}>
            <Button
              onClick={saveClickHandler}
              variant="contained"
              size="small"
              endIcon={<SaveIcon />}
            >
              Guardar
            </Button>
            <Button
              onClick={cancelNewPublicationHandler}
              variant="text"
              size="small"
              color="error"
            >
              Cancelar
            </Button>
          </Box>
        ) : (
          <Box
            sx={{
              display: "flex",
              width: "100%",
              flexDirection: { xs: "column-reverse", md: "row" },
              marginBottom: "1rem",
            }}
          >
            <Box sx={{ flex: "1" }}>
              <Box
                sx={{
                  display: "flex",
                  gap: { xs: "0.2rem", md: "1rem" },
                  flexWrap: "wrap",
                  flexDirection: { xs: "column-reverse", md: "row" },
                }}
              >
                <Button
                  onClick={confirmDeleteClickHandler}
                  variant="contained"
                  color="error"
                  size="small"
                  endIcon={<DeleteIcon />}
                >
                  Borrar
                </Button>
                <Button
                  onClick={updateClickHandler}
                  variant="contained"
                  size="small"
                  endIcon={<SaveIcon />}
                >
                  Actualizar
                </Button>

                <Divider
                  sx={{ display: { xs: "none", md: "block" } }}
                  orientation="vertical"
                  flexItem
                />

                <Button
                  onClick={() => selectPublication(null)}
                  variant="text"
                  color="error"
                  size="small"
                >
                  Cancelar
                </Button>
              </Box>
            </Box>

            {publication?.state === STATUS.DRAFT ? (
              <Button
                onClick={() => publishClickHandler(true)}
                variant="text"
                color="success"
                size="small"
                endIcon={<PublishIcon />}
              >
                Publicar
              </Button>
            ) : (
              <Button
                onClick={() => publishClickHandler(false)}
                variant="text"
                color="warning"
                size="small"
                endIcon={<PublishIcon sx={{ transform: "rotate(180deg)" }} />}
              >
                Despublicar
              </Button>
            )}
          </Box>
        )}
      </Stack>

      <Modal
        open={confirmDelete}
        onClose={() => setConfirmDelete(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: "absolute" as "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            maxWidth: "30rem",
            width: "100%",
            bgcolor: "background.paper",
            boxShadow: "2.4rem",
            p: 4,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            gap: 2,
          }}
        >
          <Typography
            id="modal-modal-title"
            align="center"
            variant="h6"
            component="h2"
          >
            ¿Estás seguro de querer borrar {publication?.title}?
          </Typography>
          <Stack spacing={2} direction="row" justifyContent="end">
            <Button onClick={() => setConfirmDelete(false)}>Cancelar</Button>
            <Button color="error" onClick={deleteClickHandler}>
              Confirmar
            </Button>
          </Stack>
        </Box>
      </Modal>
    </Box>
  );
};
