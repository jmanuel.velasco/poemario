import { Box, TextField, Typography } from "@mui/material";
import {
  ChangeEvent,
  Dispatch,
  SetStateAction,
  useContext,
  useEffect,
} from "react";
import { ApplicationContext } from "../../../contexts/ApplicationContext/ApplicationContext";
import { UIContext } from "../../../contexts/UIContext/UIContext";
import { Musa } from "../../../domain/Musa";
import { Publication } from "../../../domain/Publication";
import { getPublicationsByAuthor } from "../../../infrastructure/services/publications";
import { MusaFormField } from "../../Musas/MusaFormField";
import { MusaItem } from "../../Musas/MusaItem";

interface FieldsProps {
  formValues: any;
  setFormValues: Dispatch<SetStateAction<Publication>>;
}

export const Fields = ({ formValues, setFormValues }: FieldsProps) => {
  const { applicationState, selectPublication, setPublications } =
    useContext(ApplicationContext);
  const { displayCreateMusaForm } = useContext(UIContext);

  const { publication, author } = applicationState;

  const handlePublicationChanges = ({
    target,
  }: ChangeEvent<HTMLInputElement>) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value,
    });
  };

  useEffect(() => {
    if (publication?.musa === undefined) {
      getPublicationsByAuthor(author).then((publications) => {
        if (publications.data) {
          setPublications(publications.data);
        }
      });
    }
  }, [author, publication?.musa, selectPublication, setPublications]);

  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: 1 }}>
      <TextField
        id="outlined-basic"
        label="Título"
        name="title"
        value={formValues.title}
        onChange={handlePublicationChanges}
        fullWidth
      />
      <TextField
        sx={{
          maxHeight: { xs: "calc(100vh)", md: "calc(100vh - 30rem)" },
          overflow: "auto",
        }}
        id="outlined-multiline-flexible"
        multiline
        minRows={5}
        name="body"
        value={formValues.body}
        onChange={handlePublicationChanges}
        fullWidth
      />

      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          gap: "1rem",
        }}
      >
        <Typography
          variant="overline"
          sx={{
            maxWidth: "10rem",
            minWidth: "10rem",
          }}
        >
          <strong>¿Quién es tu musa?</strong>
        </Typography>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            gap: "0.2rem",
            justifyContent: "start",
          }}
        >
          {publication?.musa?.map((musaItem: Musa) => {
            if (!musaItem) return;

            return <MusaItem key={musaItem.id} musa={musaItem} />;
          })}
          {!displayCreateMusaForm && (
            <MusaItem musa={{ id: 0, name: "añadir" }} />
          )}
        </Box>
      </Box>

      {displayCreateMusaForm && <MusaFormField />}
    </Box>
  );
};
