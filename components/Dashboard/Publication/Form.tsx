import { Alert, AlertTitle, Box, Divider } from "@mui/material";
import { useCallback, useContext, useEffect, useState } from "react";
import { ApplicationContext } from "../../../contexts/ApplicationContext/ApplicationContext";
import { UIContext } from "../../../contexts/UIContext/UIContext";
import { Publication } from "../../../domain/Publication";
import { Controls } from "./Controls";
import { Fields } from "./Fields";
import { defaultPublicationData } from "../../../infrastructure/services/publications";

export const Form = () => {
  const { applicationState, hasErrors } = useContext(ApplicationContext);
  const { isNewPublication, displayCreateMusaForm, setDisplayCreateMusaForm } =
    useContext(UIContext);

  const { publication, author } = applicationState;

  const showForm = publication || isNewPublication;

  const [formValues, setFormValues] = useState<Publication>(
    defaultPublicationData(author)
  );

  const setPublicationValues = useCallback((publication: Publication) => {
    setFormValues((prevFormValues) => ({
      ...prevFormValues,
      title: publication.title,
      body: publication.body.replace(/<br[ /]*>/g, "\n"),
    }));
  }, []);

  const resetFormValues = useCallback(() => {
    setFormValues(defaultPublicationData(author));
  }, [author]);

  useEffect(() => {
    setDisplayCreateMusaForm(false);
    if (publication) {
      setPublicationValues(publication);
    } else {
      resetFormValues();
    }
  }, [
    publication,
    setDisplayCreateMusaForm,
    setPublicationValues,
    resetFormValues,
  ]);

  return showForm ? (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
        marginTop: "1rem",
      }}
    >
      <Box sx={{ flex: 1 }}>
        <Fields formValues={formValues} setFormValues={setFormValues} />
        <Divider sx={{ margin: "1rem" }} />
        {hasErrors && (
          <Alert severity="error" sx={{ marginBottom: "2rem" }}>
            <AlertTitle>Error</AlertTitle>
            <ul>
              {Array.from(applicationState.errors.entries(), ([key, error]) => (
                <li key={error}>{error}</li>
              ))}
            </ul>
          </Alert>
        )}
      </Box>
      {!displayCreateMusaForm && <Controls formValues={formValues} />}
    </Box>
  ) : null;
};
