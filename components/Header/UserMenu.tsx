import Link from "next/link";
import { useContext, useState } from "react";

import { ApplicationContext } from "../../contexts/ApplicationContext/ApplicationContext";

import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import MuiMenu from "@mui/material/Menu";
import MuiMenuItem from "@mui/material/MenuItem";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";

export const UserMenu = () => {
  const { logout } = useContext(ApplicationContext);
  const settings: { label: string; url: string; clickEventFn?: () => void }[] =
    [
      {
        label: "Profile",
        url: "/profile",
      },
      {
        label: "Logout",
        url: "/",
        clickEventFn: () => {
          logout();
        },
      },
    ];

  const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <Box sx={{ flexGrow: 0 }}>
      <Tooltip title="Open settings">
        <IconButton onClick={handleOpenUserMenu}>
          <ManageAccountsIcon />
        </IconButton>
      </Tooltip>
      <MuiMenu
        sx={{ mt: "45px" }}
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={Boolean(anchorElUser)}
        onClose={handleCloseUserMenu}
      >
        {settings.map((setting) => (
          <Link key={setting.url} href={setting.url}>
            <MuiMenuItem onClick={setting.clickEventFn}>
              <Typography textAlign="center">{setting.label}</Typography>
            </MuiMenuItem>
          </Link>
        ))}
      </MuiMenu>
    </Box>
  );
};
