import AppBar from "@mui/material/AppBar";
import Container from "@mui/material/Container";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Link from "next/link";
import { useContext } from "react";
import { ApplicationContext } from "../../contexts/ApplicationContext/ApplicationContext";
import { HamburguerMenu } from "./HamburguerMenu";
import { HorizontalMenu } from "./HorizontalMenu";
import { Logo } from "./Logo";
import { UserMenu } from "./UserMenu";

function HeaderComponent() {
  const { applicationState } = useContext(ApplicationContext);
  const { author } = applicationState;
  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Logo />
          <Link href="/">
            <Typography
              variant="h6"
              noWrap
              component="a"
              sx={{
                mr: 2,
                display: { xs: "none", md: "flex" },
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "#a3ab8f",
                textDecoration: "none",
                cursor: "pointer",
              }}
            >
              Amimusa
            </Typography>
          </Link>

          <HamburguerMenu />
          <HorizontalMenu />
          <Typography
            variant="h6"
            noWrap
            component="a"
            sx={{
              mr: 2,
              display: { xs: "none", md: "flex" },
              fontFamily: "monospace",
              fontWeight: 200,
            }}
          >
            Hola, {author.nombre}
          </Typography>
          <UserMenu />
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default HeaderComponent;
