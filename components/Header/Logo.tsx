import { Box } from "@mui/material";

export const Logo = () => {
  return (
    <Box
      component="img"
      sx={{ display: { xs: "none", md: "flex" }, mr: 1, width: "2rem" }}
      alt="logo"
      src="/img/logo.png"
    />
  );
};
