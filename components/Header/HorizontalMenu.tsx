import Link from "next/link";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { menuItems } from "./menuItem";

export const HorizontalMenu = () => {
  return (
    <>
      <Box
        component="img"
        sx={{ display: { xs: "flex", md: "none" }, mr: 1, width: "2rem" }}
        alt="logo"
        src="/img/logo.png"
      />
      <Link href="/">
        <Typography
          variant="h5"
          noWrap
          component="a"
          sx={{
            mr: 2,
            display: { xs: "flex", md: "none" },
            flexGrow: 1,
            fontFamily: "monospace",
            fontWeight: 700,
            letterSpacing: ".3rem",
            color: "#a3ab8f",
            textDecoration: "none",
            cursor: "pointer",
          }}
        >
          Amimusa
        </Typography>
      </Link>
      <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
        {menuItems.map((item) => (
          <Link key={item.url} href={item.url}>
          <Button
            key={item.label}
            sx={{ my: 2, color: "white", display: "block" }}
          >
            {item.label}
          </Button>
          </Link>
        ))}
      </Box>
    </>
  );
};
