import KeyboardDoubleArrowDownIcon from "@mui/icons-material/KeyboardDoubleArrowDown";
import { Box, Button, Stack, Typography } from "@mui/material";
import { useRef } from "react";
import { useScroll } from "../hooks/useScroll";
import publicationStyles from "../styles/publications.module.css";
import Link from "next/link";

export const PresentationComponent = () => {
  const containerRef = useRef<HTMLDivElement>(null);

  const { displayScroller, scrollerTop, scrollerLeft, handleScroll } =
    useScroll(containerRef.current as HTMLDivElement);
  return (
    <>
      <Box
        style={{
          maxWidth: "50rem",
          backgroundColor: "rgba(255, 255, 255, 0.2)",
          padding: "1rem",
          borderRadius: "1rem",
          fontFamily: "BohemianTypewriter",
          fontSize: "1rem",
          margin: "1rem 0.5rem 0 0.5rem",
        }}
        ref={containerRef}
        className={`${publicationStyles.hideScrollBar} ${publicationStyles.container}`}
        onScroll={handleScroll}
      >
        <p style={{ marginTop: 0 }}>
          Amimusa es un espacio dedicado a la creatividad y la expresión
          poética, todo en honor a la musa que te guía en tu proceso creativo.
          Podrás conectar con tu musa y explorar la belleza de la poesía.
        </p>

        <p>
          En Amimusa, creemos que la musa es la guía más importante en el
          proceso creativo de un poeta. Es por eso que nuestro sitio está
          diseñado para honrar a tu musa.
        </p>

        <p>
          Te invitamos a unirte a nuestra comunidad y explorar el poder de la
          musa en el mundo de la poesía. Nuestra comunidad de poetas te
          permitirá conectarte con personas de todo el mundo que comparten tu
          pasión por la poesía y las musas.
        </p>
        <p>
          Deja que Amimusa sea tu hogar poético y que tu musa te guíe en el
          proceso creativo.
        </p>
        <p style={{ textAlign: "center", fontSize: "1.3rem" }}>
          ¡Bienvenid@s a Amimusa, el hogar de los poetas y sus musas!
        </p>
        <Box
          sx={{
            display: "flex",
            flexDirection: { xs: "column", md: "row" },
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
            zIndex: 1,
            margin: "0.5rem 0",
            gap: "0.2rem",
          }}
        >
          <Link href={"/musas"}>
            <Button
              variant="contained"
              sx={{ width: { xs: "100%", md: "auto" } }}
            >
              Explorar
            </Button>
          </Link>
          <Box
            sx={{
              display: "flex",
              flexDirection: { xs: "column", md: "row" },
              justifyContent: "end",
              alignItems: "center",
              gap: { xs: "0.2rem", md: "1rem" },
              width: { xs: "100%", md: "auto" },
            }}
          >
            <Link href={"/auth/register"}>
              <Button
                variant="contained"
                sx={{ width: { xs: "100%", md: "auto" } }}
              >
                Quiero unirme a la comunidad
              </Button>
            </Link>
            <Link href={"/auth"}>
              <Button
                variant="contained"
                color="secondary"
                sx={{ width: { xs: "100%", md: "auto" } }}
              >
                Entrar
              </Button>
            </Link>
          </Box>
        </Box>
      </Box>

      {displayScroller && (
        <KeyboardDoubleArrowDownIcon
          sx={{
            position: "fixed",
            bottom: "5.5rem",
            left: scrollerLeft,
            fontSize: "2rem",
          }}
          color="primary"
        />
      )}
    </>
  );
};
