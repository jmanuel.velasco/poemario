
import { useRouter } from "next/router";
import { useContext, useEffect } from "react";
import { Grid } from "@mui/material";

import { ApplicationContext } from "../../contexts/ApplicationContext/ApplicationContext";
import { Layout } from "../../layouts/Layout";

interface Props {
  component: JSX.Element;
}

const PublicRenderWrapper = ({ component }: Props) => {
  const router = useRouter();
  const { isChecking, isUserLogged } = useContext(ApplicationContext);

  useEffect(() => {
    if (isUserLogged) {
      router.push("/musas");
    }
  }, [isUserLogged, router]);

  if (isChecking) {
    return <h1>Loading...</h1>;
  }

  return !isUserLogged ? (
    <Layout displayTitle={true}>
      <Grid item xs={12}>
        {component}
      </Grid>
    </Layout>
  ) : null;
};

export default PublicRenderWrapper;
