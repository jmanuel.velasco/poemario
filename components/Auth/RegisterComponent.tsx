
import NextLink from "next/link";
import { ChangeEvent, useContext, useState } from "react";
import { ApplicationContext } from "../../contexts/ApplicationContext";
import { Author } from "../../domain";
import { registerUser } from "../../infrastructure/services";
import PersonAddAltIcon from "@mui/icons-material/PersonAddAlt";
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  Divider,
  Link,
  TextField,
  Typography,
} from "@mui/material";

export const RegisterComponent = () => {
  const { register } = useContext(ApplicationContext);
  const [error, setError] = useState<string | null>(null);
  const [formValues, setFormValues] = useState<Author>({
    nombre: "",
    seudonimo: "",
    email: "",
    password: "",
  });

  const changeInputField = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value,
    });
  };

  const handleRegister = async () => {
    if (!formValues.nombre) {
      setError("Debes introducir un nombre");
      return;
    }
    if (!formValues.seudonimo) {
      setError("Debes introducir el seudonimo");
      return;
    }    
    if (!formValues.password) {
      setError("Debes introducir el password");
      return;
    }
    setError(null);

    const authData = await registerUser({
      userData: {
        nombre: formValues.nombre,
        seudonimo: formValues.seudonimo,
        email: formValues.email,
        password: formValues.password,
      },
    });
    if (authData.token) {
      register(authData);
    }
    if (authData.error) {
      setError(authData.error);
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        margin: "1rem 0.5rem 0 0.5rem"
      }}
    >
      {error && (
        <Alert severity="error" sx={{ marginBottom: "2rem" }}>
          <AlertTitle>Error</AlertTitle>
          <pre>{JSON.stringify(error)}</pre>
        </Alert>
      )}
      <Box
        sx={{
          maxWidth: "30rem",
          margin: "0 0.5rem",
        }}
      >
        <Typography variant="h4" gutterBottom>
          Únete a la comunidad
        </Typography>
        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { mt: 1, width: "100%" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            required
            label="Nombre"
            name="nombre"
            onChange={changeInputField}
            variant="filled"
          />
          <TextField
            label="Seudónimo"
            name="seudonimo"
            onChange={changeInputField}
            variant="filled"
          />
          <TextField
            required
            label="Email"
            name="email"
            onChange={changeInputField}
            type="email"
            variant="filled"
          />
          <TextField
            required
            label="Password"
            name="password"
            onChange={changeInputField}
            type="password"
            variant="filled"
          />
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "end",
              margin: "1rem 0",
            }}
          >
            <Button
              onClick={handleRegister}
              variant="contained"
              endIcon={<PersonAddAltIcon />}
            >
              Crear cuenta
            </Button>
          </Box>
          <Divider />
          <Typography variant="overline" display="block" gutterBottom>
            ¿Ya eres parte de la comunidad?{" "}
            <NextLink href={"/auth"} passHref>
              <Link sx={{whiteSpace: "nowrap"}}>Identíficate</Link>
            </NextLink>
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};
