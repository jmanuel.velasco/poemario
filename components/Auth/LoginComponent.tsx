import NextLink from "next/link";
import { ChangeEvent, useContext, useState } from "react";
import LoginIcon from "@mui/icons-material/Login";
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  Divider,
  Link,
  TextField,
  Typography,
} from "@mui/material";
import { ApplicationContext } from "../../contexts";
import { Author } from "../../domain/Author";
import { loginUser } from "../../infrastructure/services/loginUser";

export const LoginComponent = () => {
  const { login } = useContext(ApplicationContext);
  const [error, setError] = useState<string | null>(null);
  const [formValues, setFormValues] = useState<Author>({
    email: "",
    password: "",
  });

  const changeInputField = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value,
    });
  };

  const handleLogin = async () => {
    if (!formValues.email) {
      setError("Debes introducir un email");
      return;
    }
    if (!formValues.password) {
      setError("Debes introducir el password");
      return;
    }
    setError(null);

    const authData = await loginUser({
      email: formValues.email,
      password: formValues.password,
    });
    if (authData.token) {
      login(authData);
    } else {
      setError(authData.error);
    }
  };

  return (
    <Box
      sx={{
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        margin: "1rem 0.5rem 0 0.5rem",
      }}
    >
      {error && (
        <Alert severity="error" sx={{ marginBottom: "2rem" }}>
          <AlertTitle>Error</AlertTitle>
          <pre>{JSON.stringify(error)}</pre>
        </Alert>
      )}
      <Typography variant="h4" gutterBottom>
        Identifícate
      </Typography>
      <Box
        component="form"
        sx={{
          "& .MuiTextField-root": { mt: 1, width: "100%" },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          required
          label="Email"
          type="email"
          variant="filled"
          name="email"
          onChange={changeInputField}
        />
        <TextField
          required
          label="Password"
          type="password"
          variant="filled"
          name="password"
          onChange={changeInputField}
        />
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "end",
            margin: "1rem 0",
          }}
        >
          <Button
            onClick={handleLogin}
            variant="contained"
            endIcon={<LoginIcon />}
          >
            Entrar
          </Button>
        </Box>
      </Box>
      <Divider />
      <Typography variant="overline" display="block" gutterBottom>
        ¿Aún no perteneces a la comunidad?{" "}
        <NextLink href={"/auth/register"} passHref>
          <Link sx={{whiteSpace: "nowrap"}}>Crea una cuenta</Link>
        </NextLink>
      </Typography>
    </Box>
  );
};
