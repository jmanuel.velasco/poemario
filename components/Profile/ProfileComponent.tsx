import { useRouter } from "next/router";
import { ChangeEvent, useContext, useEffect, useState } from "react";
import SaveIcon from "@mui/icons-material/Save";
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  TextField,
  Typography,
} from "@mui/material";
import HeaderComponent from "../Header/HeaderComponent";
import { ApplicationContext } from "../../contexts/ApplicationContext";
import { Author } from "../../domain/Author";
import { updateUserProfile } from "../../infrastructure/services";

export const ProfileComponent = () => {
  const router = useRouter();
  const { isUserLogged, isChecking, applicationState, updateUserProfile: updateUserProfileStatus } =
    useContext(ApplicationContext);

  const [formValues, setFormValues] = useState<Author>({
    nombre: "",
    seudonimo: "",
  });

  const [error, setError] = useState<string | null>(null);
  const [success, setSuccess] = useState<string | null>(null);

  useEffect(() => {
    if (!isChecking) {
      const { author } = applicationState;
      if (isUserLogged && author && author.nombre && author.seudonimo) {
        setFormValues({
          nombre: author.nombre,
          seudonimo: author.seudonimo,
        });
      } else {
        router.push("/");
      }
    }
  }, [isChecking, isUserLogged, applicationState, router]);

  const changeInputField = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value,
    });
  };

  const handleUpdate = async () => {
    setError(null);
    setSuccess(null);
    const userUpdated = await updateUserProfile({
      author: {
        id: applicationState.author.id,
        nombre: formValues.nombre,
        seudonimo: formValues.seudonimo,
      },
    });
    if (userUpdated?.data) {
      setSuccess("Tu perfil ha sido actualizado.");
      updateUserProfileStatus({ author: userUpdated.data });
    } else {
      setError(userUpdated?.error!);
    }
  };

  return (
    <>
      <HeaderComponent />
      <Box sx={{ maxWidth: "30rem", margin: "1rem 0.5rem" }}>
        {error && (
          <Alert severity="error" sx={{ marginBottom: "2rem" }}>
            <AlertTitle>Error</AlertTitle>
            <pre>{JSON.stringify(error)}</pre>
          </Alert>
        )}
        {success && (
          <Alert severity="success" sx={{ marginBottom: "2rem" }}>
            <AlertTitle>Perfecto</AlertTitle>
            <pre>{JSON.stringify(success)}</pre>
          </Alert>
        )}
        <Typography variant="h4" fontFamily="BohemianTypewriter" gutterBottom>
          Actualiza tu perfil
        </Typography>
        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { width: "100%", margin: "1rem 0" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            label="Nombre"
            name="nombre"
            value={formValues.nombre}
            onChange={changeInputField}
            variant="filled"
          />
          <TextField
            label="Seudónimo"
            name="seudonimo"
            value={formValues.seudonimo}
            onChange={changeInputField}
            variant="filled"
          />
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "end",
              margin: "1rem 0",
            }}
          >
            <Button
              onClick={handleUpdate}
              variant="contained"
              endIcon={<SaveIcon />}
            >
              Actualizar
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
};
