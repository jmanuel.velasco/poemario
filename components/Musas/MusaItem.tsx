import AddToPhotosIcon from "@mui/icons-material/AddToPhotos";
import CancelIcon from "@mui/icons-material/Cancel";
import { Button, Stack } from "@mui/material";
import { useContext } from "react";
import { ApplicationContext } from "../../contexts/ApplicationContext/ApplicationContext";
import { UIContext } from "../../contexts/UIContext/UIContext";
import { Musa } from "../../domain/Musa";
import { isMusaAlreadyInPublication } from "../../infrastructure/services/publications";

interface MusaItemProps {
  musa: Musa;
  mode?: "list" | "selection";
}

export const MusaItem = ({ musa, mode = "list" }: MusaItemProps) => {
  const { applicationState, updatePublication, resetErrors } =
    useContext(ApplicationContext);

  const { publication } = applicationState;
  const { setDisplayCreateMusaForm } = useContext(UIContext);
  const { id, name } = musa;

  const buttonClickHandler = () => {
    if (mode === "list") {
      if (id) {
        removeMusaFromPublication();
      } else {
        setDisplayCreateMusaForm(true);
        resetErrors();
      }
    } else {
      addMusaToPublication();
    }
  };

  const removeMusaFromPublication = () => {
    if (publication) {
      publication.musa = publication.musa.filter(
        (musa: Musa) => musa.id !== id
      );
      updatePublication(publication);
    }
  };

  const addMusaToPublication = async () => {
    if (publication && !isMusaAlreadyInPublication({ publication, musa })) {
      publication.musa.push(musa);
      updatePublication(publication);
      setDisplayCreateMusaForm(false);
    }
  };

  return (
    <Stack spacing={2} direction="row">
      <Button
        onClick={buttonClickHandler}
        variant={!id ? "contained" : "outlined"}
        color={mode === "selection" ? "success" : "info"}
        size="small"
        startIcon={
          mode === "list" ? !id ? <AddToPhotosIcon /> : <CancelIcon /> : null
        }
      >
        {name}
      </Button>
    </Stack>
  );
};
