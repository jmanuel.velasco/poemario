import { Box, Button, ButtonGroup, TextField } from "@mui/material";
import {
  ChangeEvent,
  KeyboardEvent,
  useContext,
  useEffect,
  useState,
} from "react";
import { useDebouncedCallback } from "use-debounce";
import { ApplicationContext } from "../../contexts/ApplicationContext";
import { UIContext } from "../../contexts/UIContext";
import { Musa } from "../../domain";
import {
  createMusa,
  getAllMusas,
  isMusaAlreadyInPublication,
} from "../../infrastructure/services";
import { MusaItem } from "./MusaItem";

export const MusaFormField = () => {
  const { applicationState, updatePublication, addError, resetErrors, addMessage } =
    useContext(ApplicationContext);
  const { setDisplayCreateMusaForm } = useContext(UIContext);

  const { publication } = applicationState;

  const [musas, setMusas] = useState<Musa[]>([]);
  const [matchingMusas, setMatchingMusas] = useState<Musa[]>([]);
  const [newMusa, setNewMusa] = useState("");

  const handleSearch = useDebouncedCallback(() => {
    const matchings = musas.filter(({ name }) => {
      return name.toLowerCase().indexOf(newMusa.toLowerCase()) === 0;
    });
    setMatchingMusas(matchings);
  }, 200);

  const handleMusaChanges = ({ target }: ChangeEvent<HTMLInputElement>) => {
    resetErrors();
    setNewMusa(target.value);
    handleSearch();
  };

  const handleKeyPress = ({ key }: KeyboardEvent<HTMLInputElement>) => {
    if (key === "Enter") {
      const selectedMusa = matchingMusas.find(
        (musa: Musa) => musa.name === newMusa
      );
      if (!selectedMusa) {
        createMusaClickHandler();
      } else {
        if (
          publication &&
          !isMusaAlreadyInPublication({ publication, musa: selectedMusa })
        ) {
          publication.musa.push(selectedMusa);
          updatePublication(publication);
          setMusas([...musas, selectedMusa]);
          setDisplayCreateMusaForm(false);
        }
      }
    }
  };

  const createMusaClickHandler = async () => {
    const newMusaData = await createMusa({ name: newMusa });

    if (newMusaData.data) {
      publication?.musa.push(newMusaData.data);
      updatePublication(publication);
      setMusas([...musas, newMusaData.data]);
      setDisplayCreateMusaForm(false);
    } else {
      if (newMusaData.error! === "JWT expired") {
        addMessage("Sesión expirada");
      }
      addError(newMusaData.error!);
    }
  };

  const cancelClickHandler = () => {
    setDisplayCreateMusaForm(false);
  };

  useEffect(() => {
    const getAllMusasFn = async () => {
      const allMusasList = await getAllMusas();
      if (allMusasList.data) {
        setMusas(allMusasList.data);
        setMatchingMusas(allMusasList.data);
      } else {
        addError(allMusasList.error!);
      }
    };
    getAllMusasFn();
  }, [addError]);

  return (
    <>
      <Box
        sx={{
          display: "flex",
          margin: "1rem",
          gap: "1rem",
        }}
      >
        <TextField
          variant="standard"
          size="small"
          label="A mi musa"
          name="musa"
          onChange={handleMusaChanges}
          onKeyUp={handleKeyPress}
          fullWidth
          autoFocus
        />
        <ButtonGroup>
          <Button
            onClick={createMusaClickHandler}
            variant="contained"
            size="small"
          >
            Añadir
          </Button>
          <Button
            onClick={cancelClickHandler}
            variant="contained"
            color="error"
            size="small"
          >
            Cancelar
          </Button>
        </ButtonGroup>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          gap: "0.2rem",
          flexWrap: "wrap",
        }}
      >
        {matchingMusas &&
          matchingMusas.map((musa) => (
            <MusaItem key={musa.id} musa={musa} mode="selection" />
          ))}
      </Box>
    </>
  );
};
