import { useEffect } from "react";
import { Musa } from "../../domain/Musa";
import musasCloud from "../../styles/musas_cloud.module.css";

export interface MusasCloudProps {
  musas: Musa[];
}

export const MusasCloudComponent = ({ musas }: MusasCloudProps) => {
  useEffect(() => {
    try {
      TagCanvas.Start("tagcanvas", "taglist", {
        textFont: '"Trebuchet MS, Helvetica, sans-serif',
        maxSpeed: 0.05,
        minSpeed: 0.01,
        textColour: "#aaa",
        textHeight: 30,
        outlineMethod: "colour",
        fadeIn: 800,
        outlineColour: "#1a82f7",
        outlineOffset: 0,
        depth: 0.97,
        wheelZoom: false,
        reverse: true,
        shuffleTags: true,
        initial: [0, 0.1],
        clickToFront: 600,
        shape: "hcylinder",
        freezeActive: true,
        lock: "x",
      });
    } catch (error) {
      console.log("Canvas error.");
      if (error instanceof Error) {
        console.log(error.message);
      }
    }
  });


  return (
    <>
      <div className={`${musasCloud.tagcanvas}`}>
        <canvas id="tagcanvas" width="1000" height="600"></canvas>
      </div>
      <div id="taglist">
        <ul>
          {musas.map((musa) => (
            <li key={musa.id}>
              <a href={`/musas/${musa.id}`}>{musa.name}</a>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};
