import {generateSlug} from '../../lib/string.utils';

describe('String utilities', () => {
    test('Should return slug version from a regular string', () => {
        const slug = generateSlug('Esto es un string');
        expect(slug).toBe('esto-es-un-string');
    });
});
