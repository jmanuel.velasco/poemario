import { fireEvent, render, renderHook } from "@testing-library/react";
import { useScroll } from "../../hooks/useScroll";

describe("useScroll", () => {

  const scrollHeight = 2392;
  const clientHeight = 767;
  const scrollTop = scrollHeight - clientHeight;
  
  test("should return displayScroller false if the scroll is at the bottom", () => {
    const { container } = render(<div></div>);

    jest.spyOn(container, "scrollHeight", "get").mockImplementation(() => scrollHeight);
    jest.spyOn(container, "clientHeight", "get").mockImplementation(() => clientHeight);

    const { result } = renderHook(() => useScroll(container));

    container.addEventListener("scroll", result.current.handleScroll);

    fireEvent.scroll(container, { target: { scrollTop } });
    expect(result.current.displayScroller).toBeFalsy();
  });

  test("should return displayScroller true if the scroll is not at the bottom", () => {
    const { container } = render(<div></div>);

    jest.spyOn(container, "scrollHeight", "get").mockImplementation(() => scrollHeight);
    jest.spyOn(container, "clientHeight", "get").mockImplementation(() => clientHeight);

    const { result } = renderHook(() => useScroll(container));

    container.addEventListener("scroll", result.current.handleScroll);

    for (let i = 0; i < scrollTop; i++) {
      fireEvent.scroll(container, { target: { scrollTop: i } });
      expect(result.current.displayScroller).toBeTruthy();
    }
  });
});
