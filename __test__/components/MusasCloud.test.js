import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { faker } from "@faker-js/faker";
import { MusasCloudComponent } from "../../components/Musas/MusasCloudComponent";

const MusasStubMocker = [
  {
    id: 1,
    name: faker.random.word(),
  },
  {
    id: 2,
    name: faker.random.word(),
  },
  {
    id: 3,
    name: faker.random.word(),
  },
];

describe("MusasCloud component", () => {
  test("Should render the MusasCloud component", () => {
    render(<MusasCloudComponent musas={MusasStubMocker} />);

    const link1 = screen.getByRole("link", {name: MusasStubMocker[0].name});
    const link2 = screen.getByRole("link", {name: MusasStubMocker[1].name});
    const link3 = screen.getByRole("link", {name: MusasStubMocker[2].name});
    
    expect(link1).toHaveAttribute("href", `/musas/${MusasStubMocker[0].id}`);
    expect(link2).toHaveAttribute("href", `/musas/${MusasStubMocker[1].id}`);
    expect(link3).toHaveAttribute("href", `/musas/${MusasStubMocker[2].id}`);
    
  });
});
