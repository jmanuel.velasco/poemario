import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { faker } from "@faker-js/faker";
import { ApplicationContext } from "../../../contexts/ApplicationContext/ApplicationContext";
import { PublicationComponent } from "../../../components/Publications/PublicationComponent";
import { generateSlug } from "../../../lib/string.utils";
import { useScroll } from "../../../hooks/useScroll";

jest.mock("../../../hooks/useScroll");

const title = faker.random.word(2);
const PublicationStubMocker = {
  slug: generateSlug(title),
  title,
  body: faker.lorem.paragraph(7),
  musa: faker.random.word(),
};
const scrollIconIdentifier = "KeyboardDoubleArrowDownIcon";

describe("Publications component", () => {
  test("Should render the publication with scroller", () => {
    useScroll.mockReturnValue({
      displayScroller: true,
    });
    render(
      <ApplicationContext.Provider
        value={{
          applicationState: {
            publication: PublicationStubMocker,
          },
        }}
      >
        <PublicationComponent />
      </ApplicationContext.Provider>
    );

    expect(screen.getByText(PublicationStubMocker.title)).toBeTruthy();
    expect(screen.getByText(PublicationStubMocker.body)).toBeTruthy();
    expect(screen.getByTestId(scrollIconIdentifier)).toBeTruthy();
  });

  test("Should render the publication without scroller", () => {
    useScroll.mockReturnValue({
      displayScroller: false,
    });
    render(
      <ApplicationContext.Provider
        value={{
          applicationState: {
            publication: PublicationStubMocker,
          },
        }}
      >
        <PublicationComponent />
      </ApplicationContext.Provider>
    );

    expect(screen.getByText(PublicationStubMocker.title)).toBeTruthy();
    expect(screen.getByText(PublicationStubMocker.body)).toBeTruthy();
    expect(screen.queryByTestId(scrollIconIdentifier)).toBeFalsy();
  });
});
