import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
import * as jose from "jose";

export async function middleware(request: NextRequest, response: NextResponse) {
  const token = request.cookies.get("jwt");

  if (!token) {
    return NextResponse.redirect(new URL("/", request.nextUrl));
  }

  const secret = process.env.SECRET_TOKEN || "";

  try {
    const jwt = await jose.jwtVerify(token, new TextEncoder().encode(secret));
  } catch (error) {
    if (error instanceof Error) {
      console.log(error.message);
    }

    const response = NextResponse.redirect(new URL("/", request.url));
    response.cookies.set("jwt", "", { expires: new Date(Date.now()) });
    response.cookies.set("authorId", "", { expires: new Date(Date.now()) });
    return response;
  }

  return NextResponse.next();
}

export const config = {
  matcher: ["/escritorio", "/profile"],
};
