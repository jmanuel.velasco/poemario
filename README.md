## Nombre
Poemario

## Descripción
Web social para crear una comunidad de personas a las que les gusta compartir sus escritos en forma de inspiraciones poeéticas. El concepto de la publicación gira en torno a la "musa" como fuente de inspiración. Las publicaciones se pueden ver como anónimo aunque para publicar se debe crear una cuenta.

Como stack se ha utilizado NextJs v12, el sistema de persistencia es una base de datos MySQL y la autenticación se realiza contra firebase Authentication.

Se pretende tener una clean arquirecture, para ello existe los directorios _domain_ e _infraestrcuture_, donde en _domain_ se definen las interfaces y en _infraestrucrure_ se realiza la implemementación usada en el resto de la aplicación.

Como backend se usa el sistema proporcionado por NextJs bajo `pages/api`

## Instalación

- Clonar el repositorio
- Ejecutar `yarn install`
- Renombrar `.env.template` a `.env` y actualizar los valores
  - Es necesario tener una cuenta en Firebase para poder configura el sistema de autenticación de usuaio
  - Es posible que los valores definidos en `.env` para la conexión a la base de datos MySQL se tengan que pasar a docker cuando se lanza.
- Ejecutar `docker compose up` para crear una instancia de la base de datos

## Entorno desarrollo     

- Ejecutar `yarn dev` para lanzar la aplicación en modo desarrollo.

## Roadmap

- Crear más test, especialmente de integración
- Añadir otros sistemas de autenticación mediante Firebase, como google, facebook, twitter (X)
- Implementar una funcionalidad para compartir el escrito en redes sociales
- Implementar una funcionalidad para que los lectores puedan valorar los escritos, al menos darle un like
- Integrar ChatGPT para que realice análisis de los poemas al autor, y darle la posibilidad al autor de editarlo y publicarlo junto al poema

