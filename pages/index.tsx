import type { NextPage } from "next";
import PublicRenderWrapper from "../components/Auth/PublicRenderWrapper";
import { PresentationComponent } from "../components/PresentationComponent";

const Home: NextPage = () => (
  <PublicRenderWrapper component={<PresentationComponent />} />
);

export default Home;
