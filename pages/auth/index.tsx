import type { NextPage } from "next";

import { LoginComponent } from "../../components/Auth/LoginComponent";
import PublicRenderWrapper from "../../components/Auth/PublicRenderWrapper";

const Login: NextPage = () => (
  <PublicRenderWrapper component={<LoginComponent />} />
);

export default Login;
