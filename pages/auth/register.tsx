import type { NextPage } from "next";

import PublicRenderWrapper from "../../components/Auth/PublicRenderWrapper";
import { RegisterComponent } from "../../components/Auth/RegisterComponent";

const Register: NextPage = () => (
  <PublicRenderWrapper component={<RegisterComponent />} />
);

export default Register;
