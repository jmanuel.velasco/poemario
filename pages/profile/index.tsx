import { useRouter } from "next/router";
import { useContext, useEffect } from "react";
import { ProfileComponent } from "../../components/Profile/ProfileComponent";
import { ApplicationContext } from "../../contexts/ApplicationContext/ApplicationContext";

const Profile = () => {
  const router = useRouter();
  const { isChecking, isUserLogged } = useContext(ApplicationContext);

  useEffect(() => {
    if (!isChecking && !isUserLogged) {
      router.push("/");
    }
  }, [isChecking, isUserLogged, router]);

  if (isChecking) {
    return <h1>Loading...</h1>;
  }

  return isUserLogged ? <ProfileComponent /> : null;
};

export default Profile;
