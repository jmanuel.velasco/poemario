import { JWTPayload, jwtVerify } from "jose";
import type { NextApiRequest, NextApiResponse } from "next";
import { MysqlPublicationRepository } from "../../../infrastructure";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const method = req.method;
  const body = req.body;
  const repository = new MysqlPublicationRepository();
  const { publication } = body;
  const { author } = publication;
  const { id, title, body: publicationBody, state, musa } = publication;

  const token = req.cookies.jwt as string;
  let decoded = {} as JWTPayload;
  const secret = process.env.SECRET_TOKEN ?? "";

  try {
    if (!token) {
      return res
        .status(401)
        .json({ message: "Usuario no autenticado", data: {} });
    }
    const { payload: jwtData } = await jwtVerify(
      token,
      new TextEncoder().encode(secret)
    );
    decoded = jwtData;

    if (!author.id) {
      return res.status(400).json({ message: "Bad request", data: {} });
    }

    if (decoded["id"] !== author.id) {
      return res.status(400).json({ message: "Bad request", data: {} });
    }
  } catch (error) {
    // JWT validation failed or token is invalid
    if (error instanceof Error) {
      return res.status(400).json({ message: error.message, data: {} });
    }
  }

  try {
    switch (method) {
      case "POST":
        if (!title || !publicationBody || !musa) {
          return res.status(400).json({ message: "Bad request", data: {} });
        }

        const createdPublication = await repository.create(publication);
        return res.status(201).json({ data: createdPublication });

      case "PATCH":
        if (!id || !title || !publicationBody || !musa || !state) {
          return res.status(400).json({ message: "Bad request", data: {} });
        }

        const publicationToUpdate = await repository.findById(publication);

        if (!publicationToUpdate) {
          return res
            .status(404)
            .json({ message: "La publicación no existe", data: {} });
        }
        if (publicationToUpdate?.author.id !== decoded["id"]) {
          return res
            .status(401)
            .json({ message: "Usuario no autorizado", data: {} });
        }

        await repository.update(publication);
        return res.status(200).json({ messsage: "Ok", data: publication });

      case "DELETE":
        if (!id) {
          return res.status(400).json({ message: "Bad request", data: {} });
        }

        const publicationToDelete = await repository.findById(publication);
        if (!publicationToDelete) {
          return res
            .status(404)
            .json({ message: "La publicación no existe", data: {} });
        }

        if (publicationToDelete?.author.id !== decoded["id"]) {
          return res
            .status(401)
            .json({ message: "Usuario no autorizado", data: {} });
        }

        const deletedPublication = await repository.delete(publication);
        return res.status(200).json({ mesage: "Ok", data: deletedPublication });

      default:
        break;
    }
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message, data: {} });
    }
    return res.status(400).json(error);
  }
}
