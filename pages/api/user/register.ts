import type { NextApiRequest, NextApiResponse } from "next";
import { UserCredential, createUserWithEmailAndPassword } from "firebase/auth";
import {
  MysqlAuthorRepository,
  auth,
  createToken,
} from "../../../infrastructure";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const method = req.method;

  switch (method) {
    case "POST":
      const { email, password, nombre, seudonimo } = req.body;
      if (!email || !password || !nombre || !seudonimo) {
        return res.status(400).json({ message: "Bad request", data: {} });
      }
      const repository = new MysqlAuthorRepository();

      const authFn = () =>
        createUserWithEmailAndPassword(auth, email, password);
      const parserFn = (u: UserCredential) => u.user.uid;

      try {
        const data =
          await repository.registerWithAuthProviderByEmail<UserCredential>(
            { email, password, nombre, seudonimo },
            authFn,
            parserFn
          );

        if (data) {
          const { author } = data;
          const token = await createToken({
            id: author.id as string,
          });

          return res.status(201).json({
            message: "Ok",
            data: {
              author,
              token,
            },
          });
        }
      } catch (error) {
        if (error instanceof Error) {
          if (error.message === "auth/email-already-in-use") {
            return res.status(400).json({
              message: "Ya existe un usuario con ese email",
              data: {},
            });
          }
          if (error.message === "auth/seudonimo-already-in-use") {
            return res.status(400).json({
              message: "Ya existe un usuario con ese seudonimo",
              data: {},
            });
          }

          return res.status(500).json({
            message: error.message,
            data: {},
          });
        }
      }

    default:
      break;
  }
}
