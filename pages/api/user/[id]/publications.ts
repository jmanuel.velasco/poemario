import type { NextApiRequest, NextApiResponse } from "next";
import * as jose from "jose";
import {
  MysqlAuthorRepository,
  MysqlPublicationRepository,
} from "../../../../infrastructure";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const method = req.method;
  const { id } = req.query;

  const token = req.cookies.jwt as string;
  const secret = process.env.SECRET_TOKEN ?? "";

  try {
    if (!token) {
      return res
        .status(401)
        .json({ message: "Usuario no autenticado", data: {} });
    }
    const { payload: jwtData } = await jose.jwtVerify(
      token,
      new TextEncoder().encode(secret)
    );

    if (jwtData["id"] !== id) {
      return res.status(400).json({ message: "Bad request", data: {} });
    }
  } catch (error) {
    // JWT validation failed or token is invalid
    if (error instanceof Error) {
      return res.status(400).json({ message: error.message, data: {} });
    }
  }

  switch (method) {
    case "GET":
      const publicationRepository = new MysqlPublicationRepository();
      const authorRepository = new MysqlAuthorRepository();

      try {
        const authorExists = await authorRepository.getById(id as string);
        if (!authorExists) {
          return res
            .status(404)
            .json({ message: "El usuario no existe", data: {} });
        }
        const publications = await publicationRepository.listByAuthor(
          id as string
        );
        res.status(200).json({ message: "Ok", data: publications });
      } catch (error) {
        if (error instanceof Error) {
          return res.status(500).json({ message: error.message, data: {} });
        }
      }

    default:
      break;
  }
}
