import type { NextApiRequest, NextApiResponse } from "next";
import { MysqlAuthorRepository } from "../../../../infrastructure/MysqlAuthorRepository";
import * as jose from "jose";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const repository = new MysqlAuthorRepository();

  const method = req.method;
  const { id } = req.query;

  const token = req.cookies.jwt as string;
  const secret = process.env.SECRET_TOKEN ?? "";

  try {
    if (!token) {
      return res
        .status(401)
        .json({ message: "Usuario no autenticado", data: {} });
    }
    const { payload: jwtData } = await jose.jwtVerify(
      token,
      new TextEncoder().encode(secret)
    );

    if (jwtData["id"] !== id) {
      return res.status(400).json({ message: "Bad request", data: {} });
    }
  } catch (error) {
    // JWT validation failed or token is invalid
    if (error instanceof Error) {
      return res.status(400).json({ message: error.message, data: {} });
    }
  }

  switch (method) {
    case "GET":
      try {
        const author = await repository.getById(id as string);
        return res.status(200).json({ message: "Ok", data: author });
      } catch (error) {
        if (error instanceof Error) {
          return res.status(400).json({ message: error.message, data: {} });
        }
      }

    case "PATCH":
      const { nombre, seudonimo } = req.body;
      try {
        const authorExists = await repository.getById(id as string);
        if (!authorExists) {
          return res
            .status(404)
            .json({ message: "El usuario no existe", data: {} });
        }

        const author = await repository.update({
          id: id as string,
          nombre,
          seudonimo,
        });

        res.status(200).json({ message: "Ok", data: author });
      } catch (error) {
        if (error instanceof Error) {
          if (error.message.indexOf("ER_DUP_ENTRY") >= 0) {
            return res.status(400).json({
              message: "Ya existe una autor con ese seudonimo",
              data: {},
            });
          }
          return res.status(400).json({ message: error.message, data: {} });
        }
      }

      break;

    default:
      break;
  }
}
