import type { NextApiRequest, NextApiResponse } from "next";
import { UserCredential, signInWithEmailAndPassword } from "firebase/auth";
import {
  MysqlAuthorRepository,
  auth,
  createToken,
} from "../../../infrastructure";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const method = req.method;

  switch (method) {
    case "POST":
      const { email, password } = req.body;
      if (!email || !password) {
        return res.status(400).json({ message: "Bad request", data: {} });
      }
      const repository = new MysqlAuthorRepository();

      const loginFn = () => signInWithEmailAndPassword(auth, email, password);
      const parserFn = (u: UserCredential) => u.user.uid;

      try {
        const data = await repository.login(
          { email, password },
          loginFn,
          parserFn
        );
        if (!data) {
          return res.status(400).json({ message: "Ok", data: {} });
        }

        if (data) {
          const { author } = data;
          const token = await createToken({
            id: author.id as string,
          });

          return res.status(200).json({
            message: "Ok",
            data: {
              author,
              token,
            },
          });
        }
      } catch (error) {
        if (error instanceof Error) {
          if (error.message === "auth/wrong-password") {
            return res
              .status(404)
              .json({ message: "La contraseña no es correcta", data: {} });
          }
          if (error.message === "auth/user-not-found") {
            return res
              .status(404)
              .json({ message: "No existe el usuario", data: {} });
          }
          if (error.message === "auth/too-many-requests") {
            return res.status(500).json({
              message: "El sistema está temporalmente caído",
              data: {},
            });
          }
          return res.status(500).json({
            message: error.message,
            data: {},
          });
        }
      }

      break;

    default:
      break;
  }
}
