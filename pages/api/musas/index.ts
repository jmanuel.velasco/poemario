import type { NextApiRequest, NextApiResponse } from "next";
import { MysqlMusaRepository } from "../../../infrastructure";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const method = req.method;

  switch (method) {
    case "GET":
      const repository = new MysqlMusaRepository();

      try {
        const data = await repository.listAll();
        return res.status(200).json({ message: "Ok", data });
      } catch (error) {
        if (error instanceof Error) {
          return res.status(404).json({ message: error.message, data: {} });
        }
        return res.status(404).json({ message: error, data: {} });
      }

    default:
      break;
  }
}
