import type { NextApiRequest, NextApiResponse } from "next";
import { MysqlPublicationRepository } from "../../../../infrastructure";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const method = req.method;
  const { id } = req.query;

  switch (method) {
    case "GET":
      const repository = new MysqlPublicationRepository();

      try {
        const publications = await repository.listByMusa(Number(id));
        if (publications.length === 0) {
          return res.status(404).json({ message: "No hay publicaciones para la musa seleccionada", data: [] });
        }
        res.status(200).json({ message: "Ok", data: publications });
      } catch (error) {
        if (error instanceof Error) {
          return res.status(500).json({ message: error.message });
        }
      }

    default:
      break;
  }
}
