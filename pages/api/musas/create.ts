import type { NextApiRequest, NextApiResponse } from "next";
import * as jose from "jose";
import { MysqlMusaRepository } from "../../../infrastructure";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const method = req.method;

  const token = req.cookies.jwt as string;
  const secret = process.env.SECRET_TOKEN ?? "";

  try {
    if (!token) {
      return res
        .status(401)
        .json({ message: "Usuario no autenticado", data: {} });
    }
    await jose.jwtVerify(token, new TextEncoder().encode(secret));
  } catch (error) {
    if (error instanceof Error) {
      return res.status(400).json({ message: error.message, data: {} });
    }
  }

  switch (method) {
    case "POST":
      const { name } = req.body;
      const repository = new MysqlMusaRepository();

      try {
        const data = await repository.create({ id: 0, name });
        return res.status(201).json({ data });
      } catch (error) {
        if (error instanceof Error) {
          if (error.message.indexOf("ER_DUP_ENTRY") >= 0) {
            return res
              .status(400)
              .json({ message: "Ya existe una musa con ese nombre", data: {} });
          }
          return res.status(500).json({ message: error.message, data: {} });
        }
        return res.status(500).json({ message: error, data: {} });
      }

    default:
      break;
  }
}
