import Document, { Html, Head, Main, NextScript } from "next/document";
import Script from "next/script";

class MyDocument extends Document {
  render() {
    return (
      <Html lang="es">
        <Head>
          <meta property="og:title" content="Amimusa - El hogar de los poetas y sus musas" />
          <meta
            property="og:description"
            content="Amimusa es un espacio dedicado a la creatividad y la expresión poética, todo en honor a la musa que te guía en tu proceso creativo. Podrás conectar con tu musa y explorar la belleza de la poesía."
          />
          <meta
            property="og:image"
            content="http://amimusa.net/img/background.jpg"
          />
        </Head>

        <body>
          <Main />
          <NextScript />
          <Script src="/js/tagcanvas.min.js" strategy="beforeInteractive" />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
