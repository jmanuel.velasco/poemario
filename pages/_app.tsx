import CssBaseline from "@mui/material/CssBaseline";
import {
  ThemeProvider,
  createTheme,
  responsiveFontSizes,
} from "@mui/material/styles";
// https://mui.com/material-ui/guides/interoperability/#css-injection-order
import { StyledEngineProvider } from "@mui/material/styles";
import type { AppProps } from "next/app";
import Head from "next/head";
import { ApplicationProvider } from "../contexts/ApplicationContext/ApplicationProvider";
import { UIProvider } from "../contexts/UIContext/UIProvider";
import "../styles/globals.css";

let darkTheme = createTheme({
  palette: {
    mode: "dark",
    success: {
      main: "#11C6A9", // custom button color (seafoam green)
      contrastText: "#ffffff", // custom button text (white)
    },
    secondary: {
      main: "#0070f3",
      contrastText: "#e4e4e4"
    }

  },
});

darkTheme = responsiveFontSizes(darkTheme);

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Amimusa</title>
      </Head>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={darkTheme}>
          <CssBaseline />
          <UIProvider>
            <ApplicationProvider>
              <Component {...pageProps} />
            </ApplicationProvider>
          </UIProvider>
        </ThemeProvider>
      </StyledEngineProvider>
    </>
  );
}

export default MyApp;
