import { useRouter } from "next/router";
import { useContext, useEffect } from "react";
import { DashboardComponent } from "../../components/Dashboard/DashboardComponent";
import { ApplicationContext } from "../../contexts/ApplicationContext/ApplicationContext";
import { UIContext } from "../../contexts";

const Escritorio = () => {
  const router = useRouter();
  const { isChecking, isUserLogged, selectPublication, resetMessages } =
    useContext(ApplicationContext);
  const { setIsNewPublication } = useContext(UIContext);

  useEffect(() => {
    selectPublication(null);
    setIsNewPublication(false);
    resetMessages();
  }, [selectPublication, setIsNewPublication, resetMessages]);

  useEffect(() => {
    if (!isChecking && !isUserLogged) {
      router.push("/");
    }
  }, [isChecking, isUserLogged, router]);

  if (isChecking) {
    return <h1>Loading...</h1>;
  }

  return isUserLogged ? <DashboardComponent /> : null;
};

export default Escritorio;
