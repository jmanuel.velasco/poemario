import { Container, Grid } from "@mui/material";
import { GetServerSidePropsContext } from "next";
import { useContext, useEffect, useRef, useState } from "react";
import {
  Menu,
  PublicationComponent,
  PublicationsHeader,
} from "../../components/Publications";
import { UIContext } from "../../contexts/UIContext/UIContext";
import { Musa, Publication } from "../../domain";
import { useResponsiveMenu } from "../../hooks/useResponsiveMenu";
import { MysqlMusaRepository } from "../../infrastructure";
import { Layout } from "../../layouts/Layout";
import { getPublicationsByMusa } from "../../infrastructure/services";

interface MusaPublications {
  musa: Musa;
}

const MusaPublications = ({ musa }: MusaPublications) => {
  const { setIsDashboardMenu } = useContext(UIContext);
  const container = useRef<HTMLDivElement>(null);
  const { responsiveMenu } = useResponsiveMenu(container);
  const [publications, setPublications] = useState<Publication[]>([]);
  const [isClient, setIsClient] = useState(false);

  useEffect(() => {
    setIsDashboardMenu(false);
    setIsClient(true);

    getPublicationsByMusa(musa.id).then((publications) =>
      setPublications(publications)
    );
  }, [musa.id, setIsDashboardMenu]);

  return isClient ? (
    <Layout displayTitle={false}>
      <Container ref={container}>
        <PublicationsHeader musa={musa.name} />
        <Grid container spacing={1}>
          <Grid item xs={1} md={3}>
            <Menu container={container} publications={publications} />
          </Grid>
          <Grid item xs={11} md={9}>
            <PublicationComponent isResponsive={responsiveMenu} />
          </Grid>
        </Grid>
      </Container>
    </Layout>
  ) : null;
};

export async function getStaticProps(context: GetServerSidePropsContext) {
  const id = Number(context.params?.id);
  const musaRepository = new MysqlMusaRepository();
  const musa = await musaRepository.get(id);

  if (!musa) {
    return {
      redirect: {
        destination: "/musas",
        permanent: false,
      },
    };
  }

  return {
    props: {
      musa,
    },
  };
}

export async function getStaticPaths() {
  const musaRepository = new MysqlMusaRepository();
  const musas = await musaRepository.listAll();

  const paths = musas.map((musa) => ({
    params: { id: `${musa.id}` },
  }));

  return {
    paths,
    fallback: "blocking", // false or 'blocking'
  };
}

export default MusaPublications;
