import type { GetServerSideProps } from "next";
import { Musa } from "../../domain/Musa";
import { MysqlMusaRepository } from "../../infrastructure/MysqlMusaRepository";

import { Grid } from "@mui/material";
import { useEffect, useState } from "react";
import { MusasCloudComponent } from "../../components/Musas/MusasCloudComponent";
import { Layout } from "../../layouts/Layout";

interface Props {
  musas: Musa[];
}

export const Musas = ({ musas }: Props) => {
  const [isClient, setIsClient] = useState(false);
  useEffect(() => {
    setIsClient(true);
  }, []);

  return isClient ? (
    <Layout displayTitle={true}>
      <Grid item xs={12}>
        <MusasCloudComponent musas={musas} />
      </Grid>
    </Layout>
  ) : null;
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const musaRepository = new MysqlMusaRepository();
  const musas: Musa[] | null = await musaRepository.list();

  return {
    props: {
      musas,
    },
  };
};

export default Musas;
