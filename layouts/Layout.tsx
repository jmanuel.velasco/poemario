import { Grid } from "@mui/material";
import Cookies from "js-cookie";
import Link from "next/link";
import HeaderComponent from "../components/Header/HeaderComponent";
import homeStyles from "../styles/home.module.css";
import musasCloud from "../styles/musas_cloud.module.css";
import { useContext } from "react";
import { ApplicationContext } from "../contexts";

interface Props {
  displayTitle: boolean;
  forPage?: string;
  children: JSX.Element;
}

export const Layout = ({ children, displayTitle, forPage }: Props) => {
  const { isUserLogged } = useContext(ApplicationContext);
  
  if (forPage === undefined) forPage = "home";

  const wrapperClass = (forPage: string) => {
    const wrapperClassMapper: { [key: string]: string } = {
      home: homeStyles.background,
      profile: homeStyles.colorBackground,
      escritorio: homeStyles.escritorioBackground,
    };

    return wrapperClassMapper[forPage];
  };

  return (
    <Grid className={wrapperClass(forPage)}>
      <Grid
        container
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        {isUserLogged && <HeaderComponent />}
        {displayTitle && (
          <Grid item xs={12}>
            <Link href={"/"}>
              <h1 className={`${homeStyles.pageTitle}`}>Amimusa</h1>
            </Link>
          </Grid>
        )}

        {children}

        {forPage === "home" && (
          <p className={`${musasCloud.credits}`}>&#169;Foto: Miguel Lázaro</p>
        )}
      </Grid>
    </Grid>
  );
};
