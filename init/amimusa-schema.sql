-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 22-10-2022 a las 04:39:05
-- Versión del servidor: 10.3.34-MariaDB-0+deb10u1
-- Versión de PHP: 7.0.33-0+deb9u10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
SET FOREIGN_KEY_CHECKS=0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `amimusa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contributors`
--

CREATE TABLE `users` (
  `id` varchar(255) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `seudonimo` varchar(255) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `seudonimo_UNIQUE` (`seudonimo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `likes`
--

CREATE TABLE `likes` (
  `id_publication` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `referer` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `musas`
--

CREATE TABLE `musas` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publications`
--

CREATE TABLE `publications` (
  `id` int(11) NOT NULL,
  `id_writting` int(11) DEFAULT NULL,
  `id_state` int(11) DEFAULT NULL,
  `id_author` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publications_musas`
--

CREATE TABLE `publications_musas` (
  `id_publication` int(11) NOT NULL,
  `id_musa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publications_type`
--

CREATE TABLE `publications_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `states` (`id`, `name`) VALUES (1, "Published");
INSERT INTO `states` (`id`, `name`) VALUES (2, "Draft");
INSERT INTO `states` (`id`, `name`) VALUES (3, "Deleted");

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Visites`
--

CREATE TABLE `Visites` (
  `id_publication` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `referer` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `writtings`
--

CREATE TABLE `writtings` (
  `id` int(11) NOT NULL,
  `publication_type` int(11) DEFAULT NULL,
  `title` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id_publication`,`date`,`referer`),
  ADD KEY `publication_IDX` (`id_publication`);

--
-- Indices de la tabla `musas`
--
ALTER TABLE `musas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indices de la tabla `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_FK` (`id_state`),
  ADD KEY `writting_FK` (`id_writting`);

--
-- Indices de la tabla `publications_musas`
--
ALTER TABLE `publications_musas`
  ADD PRIMARY KEY (`id_publication`,`id_musa`),
  ADD KEY `IDX_7EF2161FB72EAA8E` (`id_publication`),
  ADD KEY `IDX_7EF2161FFB53D80` (`id_musa`);

--
-- Indices de la tabla `publications_type`
--
ALTER TABLE `publications_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Visites`
--
ALTER TABLE `Visites`
  ADD PRIMARY KEY (`id_publication`,`date`,`referer`),
  ADD KEY `publication_IDX` (`id_publication`);

--
-- Indices de la tabla `writtings`
--
ALTER TABLE `writtings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `publication_type_FK` (`publication_type`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `musas`
--
ALTER TABLE `musas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT de la tabla `publications`
--
ALTER TABLE `publications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT de la tabla `publications_type`
--
ALTER TABLE `publications_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `writtings`
--
ALTER TABLE `writtings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=334;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `publications`
--
ALTER TABLE `publications`
  ADD CONSTRAINT `FK_32783AF440C1075C` FOREIGN KEY (`id_writting`) REFERENCES `writtings` (`id`),
  ADD CONSTRAINT `FK_32783AF44D1693CB` FOREIGN KEY (`id_state`) REFERENCES `states` (`id`);

--
-- Filtros para la tabla `publications_musas`
--
ALTER TABLE `publications_musas`
  ADD CONSTRAINT `FK_7EF2161FB72EAA8E` FOREIGN KEY (`id_publication`) REFERENCES `publications` (`id`),
  ADD CONSTRAINT `FK_7EF2161FFB53D80` FOREIGN KEY (`id_musa`) REFERENCES `musas` (`id`);

--
-- Filtros para la tabla `writtings`
--
ALTER TABLE `writtings`
  ADD CONSTRAINT `FK_F9A6AFF48726D6E4` FOREIGN KEY (`publication_type`) REFERENCES `publications_type` (`id`);

SET FOREIGN_KEY_CHECKS=1;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
