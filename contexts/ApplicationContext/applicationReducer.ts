import { Author } from "../../domain/Author";
import { Publication } from "../../domain/Publication";

type ApplicationReducerActions =
  | {
      type: "User - Login";
      payload: { author: Author };
    }
  | {
      type: "User - Update Profile";
      payload: { author: Author };
    }
  | {
      type: "User - Logout";
    }
  | {
      type: "Publications - Set";
      payload: { publications: Publication[] };
    }
  | {
      type: "Publication - Select";
      payload: { publication: Publication | null };
    }
  | {
      type: "Publication - Add";
      payload: { publication: Publication };
    }
  | {
      type: "Publication - Update";
      payload: { publication: Publication | null };
    }
  | {
      type: "Publication - Delete";
      payload: { publication: Publication | null };
    }
  | {
      type: "Errors - Add";
      payload: { error: string };
    }
  | {
      type: "Errors - Reset";
    }
  | {
      type: "Messages - Add";
      payload: { error: string };
    }
  | {
      type: "Messages - Reset";
    };

type ApplicationState = {
  author: Author;
  publications: Publication[];
  publication: Publication | null;
  errors: Set<string>;
  messages: Set<string>;
};

export const applicationReducer = (
  state: ApplicationState,
  action: ApplicationReducerActions
): ApplicationState => {
  switch (action.type) {
    case "User - Login":
      return {
        ...state,
        author: action.payload.author,
      };

    case "User - Logout":
      return {
        ...state,
        author: {},
      };

    case "User - Update Profile":
      return {
        ...state,
        author: action.payload.author,
      };

    case "Publications - Set":
      return {
        ...state,
        publications: action.payload.publications,
      };

    case "Publication - Select":
      return {
        ...state,
        publication: action.payload.publication,
      };
    case "Publication - Add":
      return {
        ...state,
        publications: [...state.publications, action.payload.publication],
        publication: action.payload.publication,
      };
    case "Publication - Update":
      return {
        ...state,
        publications: state.publications.map((publication: Publication) => {
          if (publication?.id === action.payload.publication?.id) {
            return action.payload.publication;
          }
          return publication;
        }),
        publication: action.payload.publication,
      };
    case "Publication - Delete":
      return {
        ...state,
        publications: state.publications.filter((publication) => {
          return publication.id !== action.payload.publication?.id;
        }),
      };
    case "Errors - Add":
      return {
        ...state,
        errors: state.errors.add(action.payload.error),
      };
    case "Errors - Reset":
      return {
        ...state,
        errors: new Set([]),
      };
    case "Messages - Add":
      return {
        ...state,
        messages: state.messages.add(action.payload.error),
      };
    case "Messages - Reset":
      return {
        ...state,
        messages: new Set([]),
      };
    default:
      return { ...state };
  }
};
