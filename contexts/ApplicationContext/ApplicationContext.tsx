import { createContext } from "react";
import { Author } from "../../domain/Author";
import { Publication } from "../../domain/Publication";

interface ApplicationContextProps {
  applicationState: {
    author: Author;
    publications: Publication[];
    publication: Publication | null;
    errors: Set<string>;
    messages: Set<string>;
  };
  login: (data: { author: Author; token: string }) => void;
  updateUserProfile: (data: { author: Author }) => void;
  logout: () => void;
  register: (data: { author: Author; token: string }) => void;
  isUserLogged: boolean;
  isChecking: boolean;
  setPublications: (publications: Publication[]) => void;
  selectPublication: (publication: Publication | null) => void;
  addPublication: (publication: Publication) => void;
  updatePublication: (publication: Publication | null) => void;
  deletePublication: (publication: Publication | null) => void;
  validatePublication: (publication: Publication) => Promise<boolean>;
  hasErrors: boolean;
  addError: (error: string) => void;
  resetErrors: () => void;
  displayMessage: boolean;
  addMessage: (message: string) => void;
  resetMessages: () => void;
  isSessionExpired: () => Promise<boolean>;
}

export const ApplicationContext = createContext<ApplicationContextProps>(
  {} as ApplicationContextProps
);
