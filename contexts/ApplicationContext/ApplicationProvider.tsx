import Cookies from "js-cookie";
import { useCallback, useEffect, useReducer, useState } from "react";
import { Author, Publication } from "../../domain";
import { getUserLogged } from "../../infrastructure/services";
import { ApplicationContext } from "./ApplicationContext";
import { applicationReducer } from "./applicationReducer";

const INITIAL_STATE = {
  author: {},
  publications: [],
  publication: null,
  errors: new Set([]),
  messages: new Set([]),
};

interface ApplicationProviderProps {
  children: JSX.Element;
}

export const ApplicationProvider = ({ children }: ApplicationProviderProps) => {
  const [applicationState, dispatch] = useReducer(
    applicationReducer,
    INITIAL_STATE
  );

  const [isChecking, setIsChecking] = useState(true);

  useEffect(() => {
    const checkUserIsLogged = async () => {
      const user = await getUserLogged();

      if (!user) {
        dispatch({ type: "User - Logout" });
        setIsChecking(false);
      } else {
        dispatch({ type: "User - Login", payload: { author: user.data } });
        setIsChecking(false);
      }
    };

    (async () => {
      await checkUserIsLogged();
    })();
  }, []);

  const logout = useCallback(() => {
    dispatch({ type: "User - Logout" });
    Cookies.remove("jwt");
    Cookies.remove("authorId");
    setIsChecking(false);
  }, []);

  const login = useCallback((data: { author: Author; token: string }) => {
    dispatch({ type: "User - Login", payload: { author: data.author } });
    Cookies.set("authorId", String(data.author.id) || "");
    Cookies.set("jwt", data.token);
    setIsChecking(false);
  }, []);

  const updateUserProfile = useCallback((data: { author: Author }) => {
    dispatch({
      type: "User - Update Profile",
      payload: { author: data.author },
    });
  }, []);

  const register = useCallback((data: { author: Author; token: string }) => {
    dispatch({ type: "User - Login", payload: { author: data.author } });
    Cookies.set("authorId", String(data.author.id) || "");
    Cookies.set("jwt", data.token);
    setIsChecking(false);
  }, []);

  const setPublications = useCallback((publications: Publication[]) => {
    dispatch({ type: "Publications - Set", payload: { publications } });
  }, []);

  const selectPublication = useCallback(
    async (publication: Publication | null) => {
      if ((await isSessionExpired()) && isUserLogged) {
        addMessage("Sesión expirada");
      } else {
        resetErrors();
        dispatch({ type: "Publication - Select", payload: { publication } });
      }
    },
    []
  );

  const updatePublication = useCallback((publication: Publication | null) => {
    dispatch({ type: "Publication - Update", payload: { publication } });
  }, []);

  const deletePublication = useCallback((publication: Publication | null) => {
    dispatch({ type: "Publication - Delete", payload: { publication } });
  }, []);

  const addPublication = useCallback((publication: Publication) => {
    dispatch({ type: "Publication - Add", payload: { publication } });
  }, []);

  const validatePublication = useCallback(
    async (publication: Publication): Promise<boolean> => {
      if (await isSessionExpired()) {
        addMessage("Sesión expirada");
        return false;
      }
      resetErrors();
      return new Promise((resolve, reject) => {
        if (!publication.title) {
          addError("El títlo no puede estar vacío");
          resolve(false);
        }
        if (!publication.musa || publication.musa.length === 0) {
          addError("Añade al menos a una musa");
          resolve(false);
        }
        resolve(true);
      });
    },
    []
  );

  const isSessionExpired = useCallback(async () => {
    const user = await getUserLogged();
    if (!user || Object.keys(user.data).length === 0) {
      addMessage("Sesión expirada");
    }

    return !user || Object.keys(user.data).length === 0;
  }, []);

  const addError = useCallback((error: string) => {
    dispatch({ type: "Errors - Add", payload: { error } });
  }, []);

  const resetErrors = useCallback(() => {
    dispatch({ type: "Errors - Reset" });
  }, []);

  const addMessage = useCallback((error: string) => {
    dispatch({ type: "Messages - Add", payload: { error } });
  }, []);

  const resetMessages = useCallback(() => {
    dispatch({ type: "Messages - Reset" });
  }, []);

  const isUserLogged = Cookies.get("jwt") !== undefined;

  const hasErrors = applicationState.errors.size > 0;

  const displayMessage = applicationState.messages.size > 0;

  return (
    <ApplicationContext.Provider
      value={{
        applicationState,
        isUserLogged,
        isChecking,
        hasErrors,
        displayMessage,

        login,
        updateUserProfile,
        register,
        logout,
        setPublications,
        selectPublication,
        updatePublication,
        addPublication,
        deletePublication,
        validatePublication,
        addError,
        resetErrors,
        addMessage,
        resetMessages,
        isSessionExpired,
      }}
    >
      {children}
    </ApplicationContext.Provider>
  );
};
