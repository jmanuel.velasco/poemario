import { createContext } from "react";

interface UIContextProps {
  displayMenu: boolean;
  setDisplayMenu: (displayMenu: boolean) => void;
  isNewPublication: boolean;
  setIsNewPublication: (isNewPublication: boolean) => void;
  displayCreateMusaForm: boolean;
  setDisplayCreateMusaForm: (displayCreateMusaForm: boolean) => void;
  stayInPublication: boolean;
  setStayInPublication: (stayInPublication: boolean) => void;
  isDashboardMenu: boolean;
  setIsDashboardMenu: (isDashboardMenu: boolean) => void;
}

export const UIContext = createContext<UIContextProps>(
  {} as UIContextProps
);
