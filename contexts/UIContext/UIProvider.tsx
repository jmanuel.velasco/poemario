import { useState } from "react";
import { UIContext } from "./UIContext";

interface PublicationProviderProps {
  children: JSX.Element;
}

export const UIProvider = ({
  children,
}: PublicationProviderProps): JSX.Element => {
  const [displayMenu, setDisplayMenu] = useState(false);
  const [isNewPublication, setIsNewPublication] = useState(false);
  const [displayCreateMusaForm, setDisplayCreateMusaForm] = useState(false);
  const [stayInPublication, setStayInPublication] = useState(false);
  const [isDashboardMenu, setIsDashboardMenu] = useState(false);

  return (
    <UIContext.Provider
      value={{
        displayMenu,
        isNewPublication,
        displayCreateMusaForm,
        stayInPublication,
        isDashboardMenu,

        setDisplayMenu,
        setIsNewPublication,
        setDisplayCreateMusaForm,
        setStayInPublication,
        setIsDashboardMenu,
      }}
    >
      {children}
    </UIContext.Provider>
  );
};
